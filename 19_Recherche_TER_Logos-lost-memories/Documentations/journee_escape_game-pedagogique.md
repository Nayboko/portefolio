# Journée Escape Game Pédagogique

Association EchoSciences : **www.echosciences-sud.fr**
Réseaux sociaux : #EscapeGamePeda

****

## Tableau Ronde
### Guillaume François, travaille au réseau Canopé Occitanie à Montpellier, développe des EG à des fins pédagogiques. Quels sont les interêts pédagogiques ?
#### La ludification
Transposer des routines de jeux dans des situations d'apprentissage, au service de compétences. On ne prends pas tous les éléments d'un jeu mais seulement ceux pouvant être moteur d'apprentissage pour les éléves, leur apportant de la motivation dans un univers fictif.
Classification de Bartle : Diagramme pour déterminer les profils des joueurs (collectionneurs, combattants, explorateurs, sociaux) sur deux axes Action<->Interaction et Monde<->Joueurs
André Tricot ( https://fr.wikipedia.org/wiki/Andr%C3%A9_Tricot )

#### Et à l'école ?
Pratique qui se développe
Nouveau support et contexte d'apprentissage, une démarche essai/erreur (où on a un feedback sur l'erreur pour mieux comprendre), collaboration sans compétition
Scoring, niveau, barre de progression, temps, quête, systeme de badge, storytelling, coopération
http://scape-enepe.fr/ et https://www.cquesne-escapegame.com

Ce n'est pas seulement les enseignants qui peuvent créer les escapes game, les élèves peuvent également mettre la main à la patte.
Démarche de formation intéressante : Design Thinking, créativité, méthode de travail itérative

Difficulté au début: Comment faire un escape game quand on a 25 élèves ? Il faut travailler plusieurs compétences, ne pas faire des délaissés, créer des sous-scénarios
Educatif != Pédagogique4
#### Les éclaireurs

### Mederic Heurtier qui a mis en place un atelier d'Xcape Room, revenir sur ce phénomène, son origine et quels sont les aspects commerciaux ?
Quand ils commencé, ils n'avaient pas beaucoup d'expérience (3 pour le présentateur) mais ont souhaité profiter de ce fait pour éviter d'être "pollué" par ce qu'ils auraient déjà pu voir.
	* Thème : Trouver une thématique qui puisse succiter de l'interêt
	* Corps du jeu/ Processur logique : Quelque chose qui intéresse les gens, qui apporte une certaine satisfaction en utilisant les différents sens (ouïe, vision, odorat ...).
	* Immersion : Il y a des gens qui viennent pour ça, pour l'ambiance, etc.

Objectifs : 
	* Plaisir (pas de ciblage particulier, il faut faire plaisir au plus grand nombre dans la mesure du possible), 
	* Coopération où chacun a son rôle à jouer, 
	* Autonomie
Contraintes: 
	* Espace limité, c'est un espace clot. Il faut les téléguider dans la salle sans pour autant les prendre par la main
	* Notion de temps, il faut que ça soit limité car les équipes s'enchaînent, que ça n'entraîne pas de frustration dû au temps etc.

	Personne n'est pas forcément d'accord sur la notion d'escape game.
	Il est d'accord avec le principe des Escape Game sous forme de jeux de plateau, où il y a des pénalités sur le temps pour les actions inutiles, ça force à réfléchir.
### Jeanne Chevillard et Valentin Labelle de l'association Délire d'Encre, qui ont crée plusieurs escapes games (médiathèques, bibliothèques, accessibles sur le web)
Concept d'Escape Game itinérants
Panique dans l'ISS: 
	* Il y a peu d'Escape Game dans les espaces non-urbains
	* Planète-Sciences-Occitanie, UniverSCiel
	* Contrainte: besoin d'immersion qui va succiter l'envie de joueur au jeu, et utiliser les émotions pour permettre l'appropriation des informations apprises. Il fallait faire quelque chose de léger pour pouvoir bouger les éléments, movibles. Faire du léger tout en gardant ce sentiment d'immession
	* Adressés surtout aux Lycéens mais peut être posé pour les Collégiens. Va permettre de revoir les notions déjà vu et supposées acquises, mettant les élèves face à leurs propres compétences. 
Panique dans la bibliothèqe: Comment les sciences vivent dans les médiathèques en occitanie?
	* Il existe des idées reçues en Sciences, développer l'esprit critique.
	25 vraies/fausses idées en Sciences (https://www.sne.fr/app/uploads/2018/10/25-vraies-fausses-id%C3%A9es-en-sciences.pdf)
	* Créer un jeu qui soit adaptable par n'importe qui en toute autonomie qui peut être repris par quelqu'un d'autre
	* Il y a un guide du Maître du jeu
	* Le concept s'est exporté à travers le monde

### Talk
	La phase de débriefing est extremement important, c'est un moment de renforcement des connaissances ou bien de découverte lorsque l'on désire comprendre les coulisses, le fonctionnement.
	L'enseignant va connaître ses élèves et savoir comment leur apporter des informations, et le médiateur va pouvoir apporter des nouvelles méthodes et donc d'autres façons d'apprendre.

	Types d'épreuves : Décodage, Manipulation, liés au Sens.
	Il y a deux types de résolutions d'énigmes : linéaires et non linéaires.
	Les enigmes non linéaires permettent de faire des divisions d'équipes, mais trop d'informations peuvent perdre les joueurs
	Les enigmes doivent être adaptés au themes, pouvoir y adapter une salle selon des contraintes techniques et de temps.


## Présentation des projets
### Adaptation au Changement climatique sur l'espace Catalna transfrontalier
Public cible: Collégiens des pyréenéees Orientales et de la diputacion de Girona
Difficulté: Deux langues (français, catalan), budget restreint
Toujours sur une phase exploratoire par un calendrier (fin mars)
### Escape Game Médical
Identifier la bactérie responsable des symptômes d'un patient et quel traitemet est le plus approprié
Mélange entre un jeu de rôle et un escape game
Elle est persuadé qu'il est important de comprendre et de participer pour mieux assimiler les connaissances.
Escape Game != Espace clo physiquement, ça peut aussi être Espace clo intellectuel (?)
Manipulation d'équipement de micro-biologie et présence de "patient" complices, et donc création d'une certaine immersion
Très bon retour du public
(19-20 Janvier à Sommiere dans le Gard)

### Atlantide
Application d'escape Game dans la ville où le joueur va apprendre l'histoire.
C'est quoi le fun? Comment on s'amuse? Il n'y a pas de réponse absolue.
C'est leur intérpréation du Jeux vidéo à travers la pédagogie.
valorisation de l'arrière pays, des savoir-faire, de la culture

### IOTA
Machine Learning, Programmation, IA, Fake News
Adaptable à partir du CM2
La difficulté est maléable en temps réel en fonction du médiateur
Il a été crée en différentes étapes, il y a eu plusieurs phases de tests dont durant un Hackathon de 5 jours, 
besoin de partenariats: On peut proposer d'accueillir l'événement

### Enquête au CBGP : le ravageur inconnu
Découverte des métiers et du fonctionnement d'un labo
Objectif: faire sortir les scientifiques de leurs murs et accueillir des collégiens sur une journée
Identifier un ravageur et trouver un moyen de le contenir
A la recherche de conseils car pleins d'idées ont émergée, mais le projet se cherche encore.

### INSc@pe INSA de Toulouse
Créer des activités pédagogiques avec des outils d'Escape Game.
Deux objectifs principaux: organiser des visites de Lycéens, que les étudiants sont capable de faire des choses plaisantes.
Un chercheur de l'INSA travaille sur un vaccin mais se contamine avec l'un de ses echantillions. On va alors transposer les étudiants dans son cerveaux et dans des salles fantasmées de son laboratoire. 

But: Développer les compétences des étudiants de Niveau 2 à partir de leurs connaissances de L1, travail collaboratif sur des prototypages.

### Muséuw Toulouse : SYR 730 Escape Game
Ce n'est pas un escape game pédagogique mais éducatif, mais plutôt un jeu pour casser les apprioris négatifs sur les musées.
Montrer quels sont les métiers qui existent dans ce milieu

### Atelier Ludosophiques : 
Développement d'une escape room avec l'UPV
Comment se construisent les preuves en Histoire?
On va pas jouer à un jeu, mais construire un jeu.
Vogovskogo : Cheminement d'un demandeur d'asile au travers de l'administration, qu'est-ce qu'il peut ressentir? 4-5 enigmes sur 15-20 minutes.

