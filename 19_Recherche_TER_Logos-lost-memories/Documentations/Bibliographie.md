# Recherches Bibliographiques

TER Master Informatique 2018-2019

**Auteurs :** Elodie TRIBOUT et Cédric PLUTA

## Bibliographie

* Thèse : Damien DJAOUTI, "Serious Game Design, Considérations thériques et techniques sur la création de jeux vidéo à vocation utilitaire", 1Vol, 330 pages, Informatique, UT3 Paul Sabatier, Lundi 28 Novembre 2011.

* Article : Claire DAVID, "Méthodes et méthodologie : de l'apprentissage universitaire", 17 pages, <hal-01134066v6>, 2015

* Article : Philippe Meirieu, "Grille d'analyse d'une séquence d'apprentissage", 4 pages, https://www.meirieu.com, 25 Octobre 2006

## Webographie

* 	Organisme : Éduscol, Ministère de l'éducation nationale et de la jeunesse, 
	Nom de la page :"Etudes, thèses, mémoires, revues scientifiques — Enseigner avec le numérique", 
	Nom de la section :"Jeux sérieux, mondes virtuels"
	Editeur : DGESco
	Dernière MàJ : Septembre 2013
	Date Consultation : Mardi 22 Janvier 2018
	URL : [http://eduscol.education.fr/numerique/dossier/apprendre/jeuxserieux/bibliographie/theses-etudes](http://eduscol.education.fr/numerique/dossier/apprendre/jeuxserieux/bibliographie/theses-etudes)
*	Organisme : Institut National Universitaire Champollion
	Nom de la page : Publications | Serious Game Research Network
	Nom de la section : Publications
	Editeur : Serious Game Research Network
	Dernière MàJ : 2016
	Date Consultation : Mardi 22 Janvier 2018
	URL : [http://seriousgameresearchnetwork.univ-jfc.fr/fr/publications](http://seriousgameresearchnetwork.univ-jfc.fr/fr/publications)