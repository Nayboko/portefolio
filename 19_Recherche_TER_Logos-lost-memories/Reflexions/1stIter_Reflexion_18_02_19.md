Log : Lost Memories => J'aime bien ce titre/sous-titre
Verbes d'action : Explorer, Interviewer, Assembler, Résoudre, Conclure
 
Il a oublié l'existence de ce lieu. Il n'a pas perdu la mémoire à proprement parler. Il se révélera être un élément important de cet Univers.
 
Algèbre de Boole des fonctions logiques : Modéliser des raisonnements logiques en exprimant un "état" en fonction de conditions.
 
Au niveau de l'Univers en lui même, et du personnage en particulier, il vaut mieux se préoccuper principalement des interactions émotionnelles et narratives entre le joueur et l'avatar, permettant ainsi au joueur de s'identifier d'une certaine manière à ce dernier. Cette identification jouera principalement sur l'empathie que peut éprouver le joueur, soit sur la situation directe de son avatar, soit sur son environnement.
 
On peut développer cet Univers autours du personnage.
 
Intemporel : évite les possibles anachronismes et donc erreurs de narrations qui peuvent y être liés.
 
Personnage principal : Qui est-il? Pourquoi doit-il accomplir une/des missions? Comment interagit-il avec son environnement?
 
On incarne un petit animal (qui reste à définir visuellement), enfant de la forêt
 
Objectif: Pourquoi avance t-on dans le jeu? Que se passe-t-il s'il échoue? Peut-on perdre?
https://fr.wikipedia.org/wiki/36_situations_dramatiques
 
Adjuvants : Y'en a-il? Pourquoi nous viennent-ils en aide?
 
Informations générales :
 
Genre : Exploration/Plateforme, Enquête, Réflexion
Joueurs : Solo
Support : PC
Cible : Joueurs adultes, étudiants
 
 
Concept :
 
On incarne Logan, un enfant de la forêt (animal mignon) qui se réveille dans un lieu inconnu (une grotte). Il va devoir explorer rapidement son environnement
 
En sortant,il se sent désorienté, se rendant compte qu'il n'est plus dans sa forêt natale. L'endroit est sombre et inquiétant, et seul un autre personnage semble là, à le regarder, sans mot dire, sans être menaçant, et avec intérêt. Celui-ci l'accompagnera toute l'aventure. Il rencontrera d'autres personnages avec lesquels on pourra dialoguer. Il tombera sur des passages où il faudra répondre à une énigme, suivant les énoncés de faits qu'il aura pu cumuler après avoir résolu des énigmes ou discuté avec d'autres personnages. Il devra remplir des slots à l'aide de ce qu'il a cumulé. La traversée d'un passage est la symbolisation d'une réduction de son amnésie et un pas de plus vers la "Vérité". 
 
Le jeu est divisé en 3 chapitres correspondants à l'apprentissage d'une logique. Ils sont eux-mêmes divisés en plusieurs scènes correspondants à l'apprentissage de plusieurs notions de la logique en question.
 
Condition de Victoire / Défaite :
 
Objectif : Sortir de cet endroit, découpé en une succession de scène où il faut résoudre plusieurs petites énigmes afin de collecter des éléments que l'on peut connecter entre eux afin d'ouvrir des passages.
 
Défaite : Il n'y a pas de game over. Logan doit assembler des éléments qu'il acquière afin de résoudre l’énigme de chaque scène.
 
Gameplay :
 
 
Ambiance / Lore :
 
Successions d'environnements d'abord sombres et brumeux, puis plus verdoyants et éclairés. Le personnage principal est plutôt avec des couleurs claires qui contrastent avec le background. Les autres animaux sont plus ternes mais comme figés dans le temps. Ils évoluent en fonction du background. Celui qui accompagne ressemble un peu à Totoro dans l'idée.
 
Logan n'a pas de souvenir immédiat de l'Univers dans lequel il est tombé, bien que tout lui semble familier.
 
Jeu de plateau : 2 à 4 joueurs
 
On dispose d'un plateau correspondant à une scène. Il est subdivisé en plusieurs tuiles face cachés, avec un point de départ pour le joueur et un point de sortie caché sur le plateau.
Il dispose d'un ensemble de cartes ACTION qui sont les suivantes : Se Déplacer, Parler, Observer, Utiliser, Écouter, Interagir, Combiner, Résoudre, Ramasser, Valider
Le joueur va devoir se déplacer sur le plateau afin de découvrir les tuiles environnantes. On retournera les 8 tuiles voisines à chaque déplacement (correspondant au rayon de perception) qui deviendront "connues". Il ne peut interagir qu'avec les tuiles adjacentes. Plusieurs personnages peuvent être présents sur une tuile, ayant une utilité ou seulement à des fins narratives. 
 
Chapitre 1 : Algèbre de boole
 
Scène 1 : ET - OU - NON
 
matériel disponible :
    - un plateau
    - 36 tuiles (une marqué avec une étoile au dos à mettre en position (4;4), Logan et ses compagnons en (2;4), deux traits au dos si sur les bords, sinon 1 trait)
    - une carte énigme ET, OU et NON
    - Plusieurs cartes énigmes de sortie
    - Jetons : vert(validé), bleu(vrai), rouge(faux)
    - Personnages : Logan, l'esprit, Lewis Carroll, (trouver un nom de femme logicienne)
    - cartes actions : Se Déplacer, Parler, Observer, Utiliser, Écouter, Interagir, Combiner, Résoudre, Ramasser, Valider
    - cartes symboles logiques : ET (^), OU (∨), NON (symbole correspondant)
    
Situation de départ :
    Les cartes 2 traits face cachés placés aléatoirement sur les bords du plateau, la carte étoile à mettre en position (4;4), Logan et ses compagnons en (4;2). l'esprit décide sans le révéler où se trouve les énigmes sur les bords du plateau (OU, NON, Enigme passage, la ET est forcément en (4;4)).
    
Histoire : Logan se réveille dans un endroit inconnu, sombre, froid et inquiétant. Il ne voit pas directement ce qu'il y a autours de lui. Un petit être lumineux, qui l'observait jusque là, se présente à lui. La lumière qu'il émane est suffisant pour éclairer une petite zone, permettant de percevoir à quelques pas autours de Logan.
 
 Les premières énigmes vont consister principalement en la résolution de circuits logiques. Les types de portes logiques (ET, OU et NON) doivent être symbolisé par une certaine forme afin que le joueur puisse facilement les reconnaître.
 
 Énigme du ET : Éclairer la grotte et révéler d'autres mécanismes
·         En résolvant l'énigme, une orbe va s'emplir d'énergies et  permettre de révéler le lieu dans lequel il se trouve.
·         Le joueur va se retrouver face à une stèle doté de deux slots. L'insertion de deux objets va permettre d'activer un mécanisme qui va alimenter l'orbe.
·         L'un des deux slots est occupé par une pièce rouge, le système est éteint. Il peut le récupérer.
·         L'insertion de deux pièces bleues va permettre d'activer le mécanisme, un voyant vert s'allume et ainsi l'orbe s'éclaire. 
·         L'éclairage va révéler l'emplacement de deux autres stèles situées ailleurs sur le plateau.
 
Énigme du NON : Révéler le passage
·         Une pièce bleue est placée dans un slot. Le joueur peut la récupérer.
·         Le placement d'une pièce rouge (issue de l'énigme précédente) va permettre d'alimenter le mécanisme.
 
Énigme du OU : Révéler le passage
·         Une pièce rouge est logée dans le mécanisme mais n'est pas récupérable car coincée. Le joueur peut tenter de la récupérer mais ne réussira pas.
·         Le placement de la pièce bleu, récupérée dans l’énigme précédente, et la validation de l'états des orbes va allumer un voyant parmi 3 présents sur la stèle.
Il va devoir trouver les bonnes combinaisons pour allumer les 3 voyants.
·          
La résolution de l'énigme du NON et du OU permet d'actionner un mécanisme qui va allumer l'endroit où se situe un passage pour sortir de la grotte. La résolution d'une dernière énigme, combinant ce qui a été appris précédemment (la valeur des pièces et la possibilité de les placer dans des slots), sera un peu plus complexe.
 
Énigme du passage : (Combinaison des 3 énigmes précédentes)
·         Jusqu'ici, deux slots étaient proposés. Cette fois, il va falloir insérer une pièce dans trois slots différents. Plusieurs pièces seront disposées près de la stèle. Le joueur devra trouver la solution en insérant les bonnes pièces au bon endroit.
·         L'expression correspondant à la résolution de ce mécanisme est : (NON(A) ET B ET C) ET NON(A OU D)
·         La seule solution existante est la suivante : A = 0; B = 1; C = 1; D = 0;
·         La résolution de cet énigme va permettre au joueur de traverser le passage et ainsi accéder la scène 2.
 
Scène 2 : NOR, NAND, XOR
 
