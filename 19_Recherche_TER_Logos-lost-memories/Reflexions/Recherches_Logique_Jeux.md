# La Logique et les Jeux

## Existe-t-il un lien entre la Logique et les Jeux ?

Il existe un lien très ancien entre la Logique et les Jeux, et tout particulièrement pour son enseignement. On peut prendre pour exemple les jeux dialectiques durant l'Antiquité très utilisés par Socrate puis Aristote pour apprendre à leurs élèves les différents cheminements menant à un "bon" raisonnement. Passant par le dialogue avec soi puis avec les autres au travers de questionnements, d'une investigation pourrait-on dire, on va alors chercher à accéder à une vérité commune en confrontant différentes hypothèses amenant sur une conclusion.

Suite à la fondation de la Théorie des Jeux mathématiques au début du 20ème siècle, définissant ainsi ce que sont les joueurs, leurs ensembles de stratégies et les règles du jeu, des logiciens, et particulièrement Léon Henkin, utilisent la notion de jeu afin d'apporter une sémantique à la Logique dite "infinitaire" et permettre son enseignement.

Aujourd'hui, il existe de nombreuses combinaisons de logiques et de jeux, plus particulièrement dans des domaines où la logique est appliquée, et où les jeux servent à la fois d'outil d'analyse mais également pour le divertissement.


## Quels sont les aspects de la logique pouvant être modélisés en mécaniques de jeu ?

La **logique propositionnelle** est inhérente aux jeux, et ce quelqu'ils soient, car la constitution des règles d'un jeu répondent à la même mécanique. Ainsi, la satisfiabilité d'une règle va dépendre des actions du joueur et donc d'un ensemble de faits amenant à une conclusion. 
	ex: (Le Joueur Saute) & (Il existe un trou) -> (Le Joueur meurt)

De même, les **syllogismes** peuvent être modélisé dans le sens où la constitution d'un ensemble de fait sous forme de prémisses permettent de conduire à un conclusion.

L'ensemble des perceptions et des interactions qu'un joueur peut avoir avec le jeu (son environnement) va permettre de déterminer les actions qui lui sont possibles de celles qui ne lui sont pas.

Dans un jeu où l'on peut gagner ou perdre (sans égalité) à longueur finie, il y a une nécessité de trouver des stratégies gagnantes, c'est à dire d'étudier l'ensemble des mouvements les plus rationnels face à ceux du ou des joueurs adverses, et ainsi atteindre un objectif particulier, comme la possibilité de se mettre en position gagnante. 

[x] Logique syllogistique
[x] Logique propositionnelle
[x] Logique des prédicats
[ ] Logique modale : une chose est possible, vraisemblable, probable, improbable, sûr. raisonner sur le futur, utilisé pour modéliser les logiques temporelles, pour modéliser les incertitudes et le temps. Dans les logique classique, ce qui est asserté peut être évalué, mais dans les logique modale, ce n'est pas forcément encore évaluable.
[ ] Logique combinatoire
[ ] Logique intuitionniste -> rétoré

Le jeu doit traiter de la logique de façon explicite

## Existe-t-il des genres de jeux s'adaptant particulièrement à la Logique ?

De nombreux genre de jeux traditionnels utilisent des composantes de la Logique:
* Jeux de stratégie combinatoire abstrait à information parfaite (Jeux d'Echec) ou imparfaite (Stratego)
* Jeux arithmétiques / combinatoires (Sudoku, Carré Latin)
* ...

Mais on retrouve une composante logique dans tous les genres de jeux, mais certains se prêtent beaucoup plus à l'enseignement et à l'utilisation intrinsèque de la Logique. Nous avons listé plusieurs jeux vidéos anciens comme modernes, l'Enquête et la Réflexion sont des genres récurrents que l'on peut coupler à un genre support tels que 'Action', 'Aventure' ou bien 'Gestion'. 

Suivant la nécessité de collecter des faits, des preuves et de conduire des interviews de témoins ou de suspects permettant ainsi de vérifier la satisfiabilité de scénarios, les jeux d'enquête sont tout à faire adaptés à l'apprentissage de la construction du raisonnement. On peut ainsi citer "Phoenix Wright : Ace Attorney", "L.A. Noire", "Les chevaliers de Baphomet". À cela, l'aspect narratif est extrêmement important, dépendant du public.

Les jeux de réflexion vont utiliser des mécaniques abstraites ou plus formelles, amenant le joueur à résoudre des énigmes ou des problèmes, telle que des équations par exemple. Par déduction par l'observation et la compréhension des mécaniques d'une enigme, le joueur va devoir rationnaliser les mouvements qui lui sont possibles et limités par les mécaniques du jeu.


## Pourquoi le lien Enquête-Aventure est intéressant pour un jeu logique?

L'enquête même à construire son raisonnement, comme expliqué précédemment, et l'aventure va apporter la narration nécessaire, permettant ainsi au joueur de se sentir impliqué dans l'Univers dans lequel il est plongé et qui va lui apporter les éléments nécessaire pour élucider des énigmes, tels que des indices, des rencontres, des éventuelles perceptions dans l'environnement, et également des phases contemplatives lui permettant, potentiellement, de méditer ce qu'il sait du jeu ou ce qu'il lui a apporté.

https://plato.stanford.edu/entries/logic-games/
https://lite.framacalc.org/JqGCT0OJ0S