# Réunion 07 02 2019
## Présents : Gouaich, Prince, Tribout, Pluta
## Heure début : 8 h 35
## Heure fin   : 9 h 35
## Lieu : LIRMM

But: Définir ce qui atteignable de ce qui n'est pas atteignable

Les étudiants avaient du mal de passer des phrases en langage naturel à des variables, à la résoluton du système.

Passer d'un fait à une variable => Capacité d'abstraction
Système avec des slots, une boîte avec un nom différent du contenu.
Idée des connecteurs, on peut combiner des slots pour fabriquer les fabriquer (Et, ou, non, implique)
Il faut exhiber directement les mécaniques de jeu.

Etudier la structure d'une pièce de Théâtre pour construire plus facilement le côté narratif.

Les tests de Lemoigno pourront nous aider à nous calibrer sur les besoins des étudiants.


Frustration: ça marche très bien sur les propositions (on a les variables, des règles de productions, la preuve déductive par l'enchaînement d'application de règles). Le challenge c'est d'apporter les quantificateurs.


Méthode des stripes : On va étirer le jeu pour avoir son contours, on va essayer de l'approcher via des actions du jouers sous forme de textes.
Ce qu'on voit dans un jeu, c'est l'action. Il faut repérer les verbes d'actions dans les textes.
Il faut trouver les verbes d'action signature (3-5) qui font définir le coeur du jeu. Les actions de base, l'essence du jeu.

 