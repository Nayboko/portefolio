# Reunion 31 Janvier 2019

Licence Pro IUT module programmation jeux: 
	- Séance pas possible pour nous

Choisir un axe, trouver une question à creuser
* Sociale: Qu'est ce que l'apprentissage de la logique peut lui apporter?
* Individu : Comment apprend-t-il? Cognition
* Le jeu lui-même : Comment construire un jeu implémentant des éléments d'apprentissage et des mécaniques de logique?

La logique pour le raisonnement, comment construire des preuves, lire des articles pourquoi c'est important d'apprendre à construire sa logique. Il y a un lien entre la logique et la compétence de raisonnement mathématique.

Quelle logique? Quels sont les aspects de la logique qui peuvent être capturé par les jeux? Quels sont ceux ne pouvant pas l'être.

1) Distinction entre la syntaxe, la mécanique et la sémantique, on se sépare de la forme pour garder le sens -> Logique propositionnelle -> Idée de base : Mécaniser, puis donner du sens
2) Introduction des quantificateurs et des variables -> Logique prédicative

Quelles mécaniques peuvent être modélisées dans les mécaniques de jeu? 
Que veut dire "Jeu de logique"? Y'a t-il un lien entre la logique et les jeux de Logique? Les deux "logiques" sont-elles les même? 
Y'a t-il un genre particulier qui s'adapte à la logique ? Faire un table sur les genres de ce qu'on appelle jeux de logique (Arithmétique genre Sudoku ou Enquête)

Recherche de contradiction, jeux socratiques, 

Lien intéressant à lire : Les théories de La zone proximal d'apprentissage

Théorie du flow, théorie du fun

Genre principal conseillé : Enquête dans lequel on va mobiliser la récupération de fait, 

Prochaine réunion : Tableau avec tous les jeux qualifiés considérés comme "logique" et l'analyser, mettre des catégories, puis extraire les genres qui peuvent être cohérents vis à vis du sujet. Donner des exemples de jeu qu'on connait.

ex: Pourquoi le lien Enquête-Aventure est intéressant pour un jeu logique? Qu'est ce qui est commun entre un jeu d'enquête et la logique académique.