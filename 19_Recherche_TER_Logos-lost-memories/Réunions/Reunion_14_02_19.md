# Réunion 07 02 2019
## Présents : Gouaich, Tribout, Pluta
## Heure début : 8 h 50
## Heure fin   : 
## Lieu : LIRMM

- Concept
- Preprod : Prototyper
- Prod
- Polish
- Distribution

Faire attention entre les verbes d'actions concernant le joueur et l'univers du jeu (ses mécaniques)

Enquêter:
Explorer,
Dialoguer, 

Explorer, Interviewer, Assembler, Résoudre, Conclure

Segmenter la population avec une représentant cliché de chaque segment pour vérifier qu'une mécanique/concept du jeu plait à ce stéréotype. Ce personnage type est important pour l'échantillonner.

Transformer les actions du jeu en carte (Unifier)
Ex: Une carte "Dialoguer" pour avoir le droit de parler

Situation dramatique : Monomythe de Campbel, George Polti ( https://fr.wikipedia.org/wiki/36_situations_dramatiques)

Lister les pré-requis pour jouer au jeu: Les joueurs ont une structure de logique (ils sont universitaires).

Faire une maquette plateau en jeu de carte, toute action, tout feedback est une carte, dans une situation.
