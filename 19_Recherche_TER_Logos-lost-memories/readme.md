# Logos : Lost Memories

## Résumé

C’est dans le cadre de l’exploitation et l’application de nos connaissances acquises lors de notre cursus en Master Informatique 1ère année que la réalisation de ce Travail d'Etudes et de Recherches s'incrit.

Le but était d'établir un jeu d'enquête ludique pour l'apprentissage de la logique au travers de différents puzzles et situations à résoudre.

Il s'agissait dans un premier temps d'établir le contexte, définir les problèmes de la pensée logique dans l'enseignement et chez les étudiants.

Par la suite, comprendre l'apprentissage avec des jeux et quelles sont les notions de game design importantes dans le cadre du développement d'un jeu sérieux. 

Et enfin, expliciter la phase de conception, de création et d'évaluation de notre solution.

## Notions abordées

Il s'agit principalement d'une étude épistémiologique concernant la logique enseignée en milieu universitaire.

* Etude sociologique sur la question de l'apprentissage ;

* Mise en avant du lien entre la logique et le jeu vidéo ;

* Ecriture d'un Game Design Document en suivant une certaine méthodologie (verbes d'actions, boucles de gameplay, feedbacks, etc) ;

* Etablissement de Level Design par la mise en place d'un scénario d'enquête ludique ;

* Planification du projet, étude comparative entre le planning prévisionnel et effectif.  

## En savoir plus

Allez dans le dossier "Rapport" et ouvrez le rapport qui porte le nom "logos-M1-TER2019.pdf".