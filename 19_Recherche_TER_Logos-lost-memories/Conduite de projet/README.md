# README Planning prévisionnel

Auteur(s) : Cédric PLUTA (20124156), Elodie TRIBOUT (20142504)
Sujet : N°40 http://www.lirmm.fr/~lafourcade/TERM1/doit.php?list_sujets=1#subject402

## Des ressources
N'étant que deux sur le projet, la majorité des tâches seront faites en binômes et certaines seront exécutées en parallele. Vis à vis du travail étudiant d'Elodie TRIBOUT, ne connaissant pas ses plannings à l'avance, nous ne pouvons pas définir des heures de travail précises. Nous allons nous adapter en fonction de ces contraintes et des projets inhérents aux autres matières de notre cursus.

## Des tâches
Nous avons considéré que les tâches concernant l'écriture des rapports et la préparation de l'oral du TER font parti intégrante du projet. Nous avons prévu un temps suffisament large vis à vis des examens et des contraintes énoncées plus haut.

## Des tests
Ce travail de recherche n'est pas voué à rester à l'état de projet, il sera amélioré et finalisé par la suite pour un rendu auprès de la BIU Sciences courant Octobre 2019. Le TER concerne la création d'un prototype qui subira des phases de playtest auprès d'un public de joueurs sélectionnés selon un protocole que nous allons mettre en place.