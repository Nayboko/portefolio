package utils.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

import utils.aff.Vue;
import utils.go.PointVisible;


public class ReadWritePoint {
	File rf; // Fichier en cours de lecture
	ArrayList<String> textToWrite; 
	private final static Charset ENCODING = StandardCharsets.UTF_8;
	int i=0;

	/**
	 * @brief Constructeur parametre d'un lecteur de point à partir d'un fichier
	 * @param aFileName String fichier source
	 */
	public ReadWritePoint(String aFileName) {
		rf = new File(aFileName);
		textToWrite = new ArrayList<String>();
	}

	/**
	 * @brief Lit le contenu d'un fichier point par point
	 * @param triangle boolean signifiant si le point lu appartient à un triangle ou non
	 * @return ArrayList<PointVisible> Ensemble de points
	 */
	public ArrayList<PointVisible> read(boolean triangle)  {
		ArrayList<PointVisible> points = new ArrayList<PointVisible>();
		try (Scanner scanner = new Scanner(rf, ENCODING.name())) {
			int i = 0;
			while (scanner.hasNextLine()) {
				points.add(readLine(scanner.nextLine(), i++, triangle));
			}
			//System.out.println(points.size() + " points lus");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return points;
	}

	// Suppose que les points sont decrits ligne par ligne et pour chaque ligne, le format est: 
	// x;y;label
	// x,y sont les coordonn�es et label est l'etiquette associee au point
	/**
	 * @brief Lit le contenu d'un fichier point par point
	 * @param aLine Une ligne a lire
	 * @param i Numero du point a lire
	 * @param triangle Un boolean indiquant si le point lu appartient a un triangle ou non
	 * @return PointVisible Point lu a une certaine ligne d'un fichier
	 */
	PointVisible readLine(String aLine, int i, boolean triangle) {
		Scanner scanner = new Scanner(aLine);
		scanner.useDelimiter(";");
		PointVisible p = null;
		String x,y, poid;
		i++;
		/* On supposera que les points sont decrits lignes par ligne au format suivant :
		 * x; y; poids/label
		 */
		if (scanner.hasNext()) {
			// assumes the line has a certain structure
			x = scanner.next();
			y = scanner.next();
			// Si le point appartient à un triangle
			if(triangle){
				poid = scanner.next();
				p = new PointVisible(Integer.parseInt(x)+Vue.WIDTH, Integer.parseInt(y)+50,Integer.parseInt(poid), (i-1)/3);
			}
			else{
				// Sinon appartient au Puzzle
				p = new PointVisible(Integer.parseInt(x), Integer.parseInt(y));
			}
			p.setLabel("P"+i);
		}
		scanner.close();
		return p;
	}

	public void write() {
		PrintWriter pw;
		try {
			pw = new PrintWriter(rf);
			for (String s : textToWrite) {
				pw.println(s);
				pw.flush();
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


	public void add(String s) {
		textToWrite.add(s);
	}

}
