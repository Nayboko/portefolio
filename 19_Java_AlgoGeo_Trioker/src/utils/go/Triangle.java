package utils.go;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Triangle {
	PointVisible p1, p2, p3;
	int poidsP1,poidsP2,poidsP3;
	int id; // Identifiant du triangle
	ArrayList<Triangle> aimante; // Liste des triangles rattachés à this.
	ArrayList<Etoile> Etoiles;
	private String Label_centre; 

	/**
	 * @brief Constructeur parametre de triangle
	 * @param p1 PointVisible haut a l'initialisation
	 * @param p2 PointVisible gauche a l'initialisation
	 * @param p3 PointVisible droit a l'initialisation
	 * @param id Identifiant du triangle courant
	 */
	public Triangle(PointVisible p1, PointVisible p2, PointVisible p3, int id) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		poidsP1 = p1.poids;
		poidsP2 = p2.poids;
		poidsP3 = p3.poids;
		this.id = id;
		aimante = new ArrayList<Triangle>();
		Etoiles = new ArrayList<Etoile>();
		Etoiles = calculEtoiles();
		genLabel_centre();
	}
	
	/**
	 * @brief Calcul la position en x et en y des etoiles correspondants aux poids des sommets du triangle
	 * @return Retourne la liste des étoiles, avec leurs coordonnees, correspondant au poids de chaque point du triangle
	 */
	public ArrayList<Etoile> calculEtoiles(){
		double length = (p1.distance(p2) / 32.0);
		ArrayList<Etoile> ListE = new ArrayList<Etoile>();
		DecimalFormat df = new DecimalFormat("#.#");
		
		for(PointVisible p: this.getSommets()) {
			double angle = Math.atan2(this.getCentre().y - p.y, this.getCentre().x - p.x);			
			double dist = p.distance(this.getCentre());
			if(p.poids == 1) {
				
				double origineX = (3.0/4.0) * dist * -Math.cos(angle) + this.getCentre().x;
				double origineY = (3.0/4.0) * dist * -Math.sin(angle) + this.getCentre().y;
				Etoile e = new Etoile(origineX, origineY, length);
				
				ListE.add(e);
			}else if(p.poids == 2) {
				double origineX = (3.0/4.0) * dist * -Math.cos(angle) + this.getCentre().x;
				double origineY = (3.0/4.0) * dist * -Math.sin(angle) + this.getCentre().y;
				Etoile e = new Etoile(origineX, origineY, length);
				
				ListE.add(e);
				
				origineX = (5.0/8.0) * dist * -Math.cos(angle) + this.getCentre().x;
				origineY = (5.0/8.0) * dist * -Math.sin(angle) + this.getCentre().y;
				Etoile e2 = new Etoile(origineX, origineY, length);
				
				ListE.add(e2);
			}else if(p.poids == 3) {
				double origineX = (3.0/4.0) * dist * -Math.cos(angle) + this.getCentre().x;
				double origineY = (3.0/4.0) * dist * -Math.sin(angle) + this.getCentre().y;
				Etoile e = new Etoile(origineX, origineY, length);
				
				ListE.add(e);
				if(df.format(angle).equals(df.format(Math.PI/2)) || df.format(angle).equals(df.format(-Math.PI/2))) {
					origineX = (5.0/8.0) * dist * -Math.cos(angle) + this.getCentre().x  + (1.0/16.0)*dist;
					origineY = (5.0/8.0) * dist * -Math.sin(angle) + this.getCentre().y;
					Etoile e2 = new Etoile(origineX, origineY, length);
					
					ListE.add(e2);
					
					origineX = (5.0/8.0) * dist * -Math.cos(angle) + this.getCentre().x  - (1.0/16.0)*dist;
					origineY = (5.0/8.0) * dist * -Math.sin(angle) + this.getCentre().y;
					Etoile e3 = new Etoile(origineX, origineY, length);
					
					ListE.add(e3);
				}

				if(df.format(angle).equals(df.format(5*Math.PI/6)) || df.format(angle).equals(df.format(-Math.PI/6))) {
					origineX = (5.0/8.0) * dist * -Math.cos(angle) + this.getCentre().x + (1.0/16.0)*dist;
					origineY = (5.0/8.0) * dist * -Math.sin(angle) + this.getCentre().y + (1.0/16.0)*dist;
					Etoile e2 = new Etoile(origineX, origineY, length);
					
					ListE.add(e2);
					
					origineX = (5.0/8.0) * dist * -Math.cos(angle) + this.getCentre().x - (1.0/16.0)*dist;
					origineY = (5.0/8.0) * dist * -Math.sin(angle) + this.getCentre().y - (1.0/16.0)*dist;
					Etoile e3 = new Etoile(origineX, origineY, length);
					
					ListE.add(e3);
				}
				if(df.format(angle).equals(df.format(-5*Math.PI/6)) || df.format(angle).equals(df.format(Math.PI/6))) {
					origineX = (5.0/8.0) * dist * -Math.cos(angle) + this.getCentre().x - (1.0/16.0)*dist;
					origineY = (5.0/8.0) * dist * -Math.sin(angle) + this.getCentre().y + (1.0/16.0)*dist;
					Etoile e2 = new Etoile(origineX, origineY, length);
					
					ListE.add(e2);
					
					origineX = (5.0/8.0) * dist * -Math.cos(angle) + this.getCentre().x;
					origineY = (5.0/8.0) * dist * -Math.sin(angle) + this.getCentre().y - (1.0/16.0)*dist;
					Etoile e3 = new Etoile(origineX, origineY, length);
					
					ListE.add(e3);
				}
			}
		}
		
		return ListE;
	}

	/**
	 * @brief Permet de recuperer l'identifiant du triangle courant
	 * @return int numero du triangle
	 */
	public int getID(){
		return this.id;
	}
	
	/**
	 * @brief Retourne le premier point du triangle
	 * @return PointVisible p1
	 */
	public PointVisible getP1() {
		return p1;
	}

	/**
	 * @brief Modifie le premier point du triangle
	 * @param p1 PointVisible
	 */
	public void setP1(PointVisible p1) {
		this.p1 = p1;
	}

	/**
	 * @brief Retourne le second point du triangle
	 * @return PointVisible p2
	 */
	public PointVisible getP2() {
		return p2;
	}

	/**
	 * @brief Modifie le second point du triangle
	 * @param p2 PointVisible
	 */
	public void setP2(PointVisible p2) {
		this.p2 = p2;
	}

	/**
	 * @brief Retourne le troisieme point du triangle
	 * @return PointVisible p3
	 */
	public PointVisible getP3() {
		return p3;
	}
	
	/**
	 * @brief Modifie le troisieme point du triangle
	 * @param p3 PointVisible
	 */
	public void setP3(PointVisible p3) {
		this.p3 = p3;
	}

	/**
	 * @brief Recupere le point central du triangle courant
	 * @return PointVisible un point central du triangle courant
	 */
	public PointVisible getCentre(){
		return new PointVisible((p1.x+p2.x+p3.x)/3,(p1.y+p2.y+p3.y)/3);
	}

	/**
	 * @brief Recupere le label central du triangle courant
	 * @return String Label central
	 */
	public String getLabel_centre() {
		return Label_centre;
	}
	
	public ArrayList<Etoile> getEtoiles() {
		return this.Etoiles;
	}

	/**
	 * @brief Genere le label central du triangle par composition des poids de chaque point
	 */
	public void genLabel_centre() {
		Label_centre = Integer.toString(poidsP1)+Integer.toString(poidsP2)+Integer.toString(poidsP3);
	}

	/**
	 * @brief Recupere les sommets du triangle courant
	 * @return PointVisible[] Un tableau contenant les sommets du triangle
	 */
	public PointVisible[] getSommets(){
		PointVisible[] r = new PointVisible[3];
		r[0] = p1;
		r[1] = p2;
		r[2] = p3;
		return r;
	}

	
	public void reInitAimante(ArrayList<Triangle> triangles){

		PointVisible[] sT1 = this.getSommets();
		for(Triangle t : triangles){
			if(!t.equals(this) && !aimante.contains(t) && t.isClose(this)){
				PointVisible[] sT2 = t.getSommets();
				int nbProche = 0;
				for(PointVisible p : sT1){
					for(PointVisible p2 : sT2){
						if(p.poids == p2.poids && p.distance(p2) < 2){
							nbProche++;
						}
					}
				}
				if(nbProche > 1){
					t.addAimante(this);
					this.addAimante(t);
				}
			}
		}
	}

	/**
	 * @brief Ajoute un triangle à la liste des triangles rattachés au triangle courant
	 * @param t un triangle que l'on va aimanter au triangle courant
	 */
	public void addAimante(Triangle t){
		aimante.add(t);
	}

	/**
	 * @brief Retourne la liste des triangles aimantes au triangle courant
	 * @return ArrayList<Triangle> Liste des triangles aimantes
	 */
	public ArrayList<Triangle> getAimante() {
		return this.aimante;
	}

	/**
	 * @brief Indique si un triangle est attache au triangle courant
	 * @param t un triangle
	 * @return boolean Retourne Vrai si le triangle est attache au triangle courant, Faux sinon
	 */
	public boolean isAttachedTo(Triangle t){
		return aimante.contains(t);
	}

	/**
	 * @brief Retourne Vrai si un triangle est proche du triangle courant, Faux sinon
	 * @param t Un triangle
	 * @return boolean Retourne Vrai si un triangle est proche du triangle courant, Faux sinon
	 */
	public boolean isClose(Triangle t){
		return this.getCentre().distance(t.getCentre()) < 75;
	}
	
	/**
	 * @brief Indique si un clic de souris est à l'interieur du triangle ou non
	 * @param x int Coordonnee en abscisse 
	 * @param y int Coordonee en ordonnée
	 * @return boolean Retourne Vrai si le clic est a l'interieur, Faux sinon
	 */
	public boolean isInside(int x, int y){
		double a = (double)((p2.y - p3.y)*(x - p3.x) + (p3.x - p2.x)*(y-p3.y))/((p2.y-p3.y)*(p1.x-p3.x)+(p3.x-p2.x)*(p1.y-p3.y));
		double b = (double)((p3.y - p1.y)*(x - p3.x) + (p1.x - p3.x)*(y-p3.y))/((p2.y-p3.y)*(p1.x-p3.x)+(p3.x-p2.x)*(p1.y-p3.y));
		double c = 1 - a - b;
		return a >= 0 && a <= 1 && b >= 0 && b <= 1 && c >= 0 && c <= 1;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof Triangle){
			Triangle t = (Triangle)o;
			return t.getID() == this.getID();
		}
		return false;
	}
	
	/**
	 * @brief Initialise la liste des triangles aimantes
	 */
	public void initAimante() {
		aimante = new ArrayList<Triangle>();
	}
	
	
}
