package utils.go;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.lang.Math;
import utils.aff.Couleur;

@SuppressWarnings("serial")
public class PointVisible extends Rectangle {

	private static int midWidth = 5;
	private Color color = Couleur.fg; // Si forPrinter(true) alors BLACK
	private String label;	
	public Point2D.Double P; // coordonnees dans le modèle
	public int poids, idTri = -1;
	public Color Color_id = Color.BLACK;

	/**
	 * @brief Constructeur par defaut du point à l'origine(0,0)
	 */
	PointVisible() {
		super(0,0);
		this.P = new Point2D.Double((double) x, (double) y);
	}

	/**
	 * @brief Constructeur parametre d'un point(x,y)
	 * @param x Coordonnee en abscisse
	 * @param y Coordonnee en ordonnée
	 */
	public PointVisible(double x, double y) {
		super((int)x, (int)y, 2 * PointVisible.midWidth, 2 * PointVisible.midWidth);
		this.P = new Point2D.Double(x, y);
		label = "p";
	}
	
	/**
	 * @brief Constructeur parametre d'un point(x,y,p)
	 * @param x Coordonnee en abscisse
	 * @param y Coordonnee en ordonnée
	 * @param p Poids associe au point
	 */
	public PointVisible(double x, double y, int p) {
		super((int)x, (int)y, 2 * PointVisible.midWidth, 2 * PointVisible.midWidth);
		this.P = new Point2D.Double(x, y);
		poids = p;
		label = "p";
	}
	
	/**
	 * @brief Constructeur parametre d'un point(x,y)
	 * @param x Coordonnee en abscisse
	 * @param y Coordonnee en ordonnée
	 * @param p Poids associe au point
	 * @param id Identifiant du triangle auquel le point est rattache
	 */
	public PointVisible(double x, double y, int p, int id) {
		super((int)x, (int)y, 2 * PointVisible.midWidth, 2 * PointVisible.midWidth);
		this.P = new Point2D.Double(x, y);
		poids = p;
		label = "p";
		idTri = id;
	}
	
	/**
	 * @brief Setter du label
	 * @param label String 
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
	/**
	 * @brief Getter du label
	 * @return String le label associe au point 
	 */
	public String getLabel(){
		return label;
	}

	/**
	 * @brief Getter de la couleur du point
	 * @return Color Couleur associé au point 
	 */
	public Color getColor_id() {
		return this.Color_id;
	}

	/**
	 * @brief Setter de la couleur du point
	 * @param color_tmp Color
	 */
	public void setColor_id(Color color_tmp) {
		this.Color_id = color_tmp;
	}
	
	/**
	 * @brief Teste l'égalite entre le point courant et un objet o
	 * @param o Un objet
	 * @return Retourne vrai si un objet 0 est égal à this, faux sinon
	 */
	public boolean equals(Object o){
		if(o instanceof PointVisible){
			PointVisible p = (PointVisible)o;
			// On compare les coordonnees, le poids ainsi que le triangle associe
			return p.getX() == this.getX() && p.getY() == this.getY() && p.poids == this.poids && p.idTri == this.idTri;
		}
		return false;
	} 
	
	/**
	 * @brief Permet de dessiner un point
	 * @param g2d Objet graphique Graphics2D dans lequel on va dessiner le point this
	 */
	public void dessine(Graphics2D g2d) {
		g2d.setColor(color);
		// Ellipse2D.Double(double x, double y, double width, double height)
		g2d.fill(new Ellipse2D.Double(this.x - midWidth, this.y - midWidth, 2 * midWidth, 2 * midWidth));
		//drawLabel(g2d);
	}

	/**
	 * @brief Permet de calculer la distance entre this et un point p
	 * @param p PointVisible 
	 * @return double Retourne la distance séparant this d'un point p
	 */
	public double distance(PointVisible p){
		// dist = SQRT((|x - px|)² + (|x - px|)²)
		double d = Math.pow(Math.abs(this.x - p.getX()),2) + Math.pow(Math.abs(this.y - p.getY()),2);
		return Math.sqrt(d);
	}

	/**
	 * @brief Affiche sur la sortie standard les caractéristiques du point courant
	 * 
	 */
	public void print() {
		System.out.println("x = " + x + " y = " + y + " w = " + width + " h = " + height);
	}

	/**
	 * @brief Dessine le poids du point courant dans la fenêtre g2d
	 * @param g2d Objet graphique Graphics2D
	 */
	public void drawLabel(Graphics2D g2d) {
		FontMetrics fm = g2d.getFontMetrics();
		String longLabel;
		int centeredText;
		longLabel = String.valueOf(poids);
		centeredText = (int) (x - fm.stringWidth(longLabel) / 2);
		g2d.drawString(longLabel, centeredText, (int) (y - midWidth - fm.getDescent()));
	}

	//return model coordinates for this point
	public Point2D.Double getMC() {
		return P;
	}

	public void copyCoords() {
		x = (int) P.x;
		y = (int) P.y;

	}
	
	public String toString() {
		return this.label;
	}



}
