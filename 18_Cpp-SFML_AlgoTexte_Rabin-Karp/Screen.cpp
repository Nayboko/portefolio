#include <iostream>
#include <unistd.h>

#include "Screen.hpp"

Screen::Screen(){
	this->screenTitle = "Ecran par défaut";
	this->Running = false;
	this->etape = 1;
	this->alpha_max = 3 * 255;
	this->alpha_div = 3;
	this->alpha = 255;
}

Screen::Screen(std::string title, std::string fPath, std::string tPath){
	this->screenTitle = title;
	this->Running = false;
	this->etape = 1;
	this->alpha_max = 3 * 255;
	this->alpha_div = 3;
	this->alpha = 255;
	std::cout << "\033[1;31m###\033[0m	Instanciation de l'écran " << screenTitle << "\t \033[1;31m###\033[0m" << std::endl;
	FontLoad(&Font, fPath);
	TextureToSprite(S_bg, T_bg, sf::Color(255,255,255,alpha), tPath, 0.66666f, 0.66666f);
}

Screen::~Screen(){}

int		Screen::run(sf::RenderWindow *&App){
	return 0;
}
	
void	Screen::draw(sf::RenderWindow &App){}

int	Screen::processEvent(sf::RenderWindow *&App){
	sf::Event Event;
	while(App->pollEvent(Event)){
		// L'user a fermé la fenêtre
		if(Event.type == sf::Event::Closed){
			App->close();
			this->setRunning(false);
			return -1;
		}

		// Si on a fait un clic de souris left
		if (Event.mouseButton.button == sf::Mouse::Left)
		{
		    std::cout << "the left button was pressed" << std::endl;
		    std::cout << "mouse x: " << Event.mouseButton.x << std::endl;
		    std::cout << "mouse y: " << Event.mouseButton.y << std::endl;
		}

		// Si on a fait un clic de souris droit
		if (Event.mouseButton.button == sf::Mouse::Right)
		{
		    std::cout << "the right button was pressed" << std::endl;
		    std::cout << "mouse x: " << Event.mouseButton.x << std::endl;
		    std::cout << "mouse y: " << Event.mouseButton.y << std::endl;
		}

	}
	return 0;
}

bool	Screen::TextureLoad(sf::Texture &t, const std::string path){

	bool resultat = false;

	// On charge l'image qui va être lié à la Texture
	if(!t.loadFromFile(path)){
		std::cerr << "Erreur dans le chargement de " << path << std::endl;
	}else{
		std::cerr << "Succès du chargement de " << path << std::endl;
		resultat = true;
	}

	return resultat;
}

bool	Screen::FontLoad(sf::Font *f, const std::string path){
	bool resultat = false;

	if(!f->loadFromFile(path)){
		std::cerr << "Erreur dans le chargement de la police de caractère." << std::endl;
	}else{
		std::cerr << "Succès du chargement de " << path << std::endl;
		resultat = true;
	}

	return resultat;
}

bool	Screen::isButtonClicked(sf::Sprite *spr, sf::RenderWindow *&App){
	// On récupère les coordonnées de la souris
	// getPosition prend en paramètre un objet de type Window
	// Or App est RenderWindow*& et Window est super de RenderWindow
	// Donc cast de App et déréférencement.
	int x = sf::Mouse::getPosition(*(sf::Window*&)App).x;
	int y = sf::Mouse::getPosition(*(sf::Window*&)App).y;

	//std::cerr << "Souris\t" << x << "\t" << y << std::endl;

	// On récupère les coordonées du Sprite
	const sf::Texture* RectSprite = spr->getTexture();
	int sizeX = RectSprite->getSize().x * 0.7;
	int sizeY = RectSprite->getSize().y * 0.4;
	int sprX = spr->getPosition().x;
	int sprY = spr->getPosition().y;

	//std::cerr << "Sprite\t" << sprX + sizeX << "\t" << sprY + sizeY << std::endl;

	// On teste si la souris est dans l'encadrement du Sprite
	if(x >= sprX && x<= sprX + sizeX
		&&
		y >= sprY && y <= sprY + sizeY
		&&
		sf::Mouse::isButtonPressed(sf::Mouse::Left)){
		return true;
	}


	return false;
}

void	Screen::setText(sf::Text &t, std::wstring s, sf::Font f, sf::Color c, unsigned int size){

	t.setFont(f);
	t.setColor(c);
	t.setCharacterSize(size);
	t.setString(s);
}

 void	Screen::positionText(sf::Text &t, float _X, float _Y){
 	t.setPosition(_X, _Y);
 }

void	Screen::TextureToSprite(sf::Sprite &s, sf::Texture &t, sf::Color c, std::string path, float _X, float _Y){	
	TextureLoad(t, path);
	s.setTexture(t);
	s.setScale(_X, _Y);
	s.setColor(c);
}

bool	Screen::getRunning()const{
	return this->Running;
}
void	Screen::setRunning(const bool b){
	this->Running = b;
}

std::string Screen::getTitle() const{
	return this->screenTitle;
}

int		Screen::getAlpha()const{
	return this->alpha;
}
void	Screen::setAlpha(int _alpha){
	this->alpha = _alpha;
}

int Screen::getEtape()const{
	return this->etape;
}

void Screen::setEtape(const int e){
	this->etape = e;
}

sf::Font Screen::getFont()const{
	return this->Font;
}