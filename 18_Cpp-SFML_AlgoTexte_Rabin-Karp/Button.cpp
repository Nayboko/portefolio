#include <iostream>
#include "Button.hpp"

Button::Button(){}
Button::Button(float posX, float posY, float scaleX, float scaleY, unsigned int txtSize, std::wstring _str, sf::Color bgColor, sf::Color txtColor, std::string fPath, std::string tPath){

	this->x = posX;
	this->y = posY;
	this->isClicked = false;
	this->str = _str;

	FontLoad(Font, fPath);
	TextureToSprite(S_button, T_button, bgColor, tPath, scaleX, scaleY);
	setText(text, str, Font, txtColor, txtSize);

	const sf::Texture* RectSprite = S_button.getTexture();
	int X_SizeSprite = RectSprite->getSize().x;
	S_button.setPosition(x - X_SizeSprite, y);
	text.setPosition(x - X_SizeSprite, y);

}

Button::~Button(){}
	
float Button::getX() const{
	return this->x;
}
void Button::setX(const float _x){
	this->x = _x;
}

float Button::getY() const{
	return this->y;
}
void Button::setY(const float _y){
	this->y = _y;
}

bool Button::getClicked() const{
	return this->isClicked;
}
void Button::setClicked(const bool b){
	this->isClicked = b;
}

std::wstring Button::getStr() const{
	return this->str;
}
void Button::setStr(const std::wstring _str){
	this->str = _str;
}

bool	Button::TextureLoad(sf::Texture &t, const std::string path){

	bool resultat = false;

	// On charge l'image qui va être lié à la Texture
	if(!t.loadFromFile(path)){
		std::cerr << "Erreur dans le chargement de " << path << std::endl;
	}else{
		std::cerr << "Succès du chargement de " << path << std::endl;
		resultat = true;
	}

	return resultat;
}

bool	Button::FontLoad(sf::Font &f, const std::string path){
	bool resultat = false;

	if(!f.loadFromFile(path)){
		std::cerr << "Erreur dans le chargement de la police de caractère." << std::endl;
	}else{
		std::cerr << "Succès du chargement de " << path << std::endl;
		resultat = true;
	}

	return resultat;
}

void	Button::TextureToSprite(sf::Sprite &s, sf::Texture &t, sf::Color c, std::string path, float _X, float _Y){	
	TextureLoad(t, path);
	s.setTexture(t);
	s.setScale(_X, _Y);
	s.setColor(c);
}


void	Button::setText(sf::Text &t, std::wstring s, sf::Font f, sf::Color c, unsigned int size){

	t.setFont(f);
	t.setColor(c);
	t.setCharacterSize(size);
	t.setString(s);
}

void Button::setTextPosition(float _X, float _Y){
	text.setPosition(_X, _Y);
}
