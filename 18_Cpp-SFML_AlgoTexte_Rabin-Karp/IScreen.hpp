#ifndef ISCREEN_HPP
#define ISCREEN_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class IScreen{
public:
	virtual int		run(sf::RenderWindow *&App) = 0;
	
	virtual void	draw(sf::RenderWindow &App) = 0;

	virtual int	processEvent(sf::RenderWindow *&App) = 0;

	virtual bool	TextureLoad(sf::Texture &t, const std::string path) = 0;

	virtual	bool	FontLoad(sf::Font *f, const std::string path) = 0;

	virtual bool	isButtonClicked(sf::Sprite *spr, sf::RenderWindow *&App) = 0;

	virtual void	setText(sf::Text &t, std::wstring s, sf::Font f, sf::Color c, unsigned int size) = 0;

	virtual void	positionText(sf::Text &t, float _X, float _Y) = 0;

	virtual void	TextureToSprite(sf::Sprite &s, sf::Texture &t, sf::Color c, std::string path, float _X, float _Y) = 0;

	virtual std::string getTitle() const = 0;

};

#endif