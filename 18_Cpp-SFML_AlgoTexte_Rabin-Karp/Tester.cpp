#include <iostream>
#include <unistd.h>

#include "Screen.hpp"
#include "Tester.hpp"
#include "RB.hpp"

Tester::Tester(std::string title, std::string fPath, std::string tPath):Screen(title, fPath, tPath){
	this->etape = 1;
	this->UserSequence = "";
	this->UserMotif = "";
	this->sizeUserSequence = 0;
	this->sizeUserMotif = 0;
	this->rb = new RB();
	setAlpha(0);
}

Tester::~Tester(){
	std::cout << "Destruction du Screen Tester" << std::endl;
	delete[] rb;
}

int Tester::run(sf::RenderWindow *&App){
	setRunning(true);
	bool	isRunningRb	= false;
	unsigned int 		menu_font_size = App->getSize().y * 0.05;   // Taille des fonts Menu
	float 		X_pos_button = App->getSize().x; // Position des bouttons en X
	float		Y_pos_button = App->getSize().y;

	Button Menu((X_pos_button * 0.45f), (Y_pos_button*0.05f), 0.2f, 0.2f, menu_font_size, L"Menu", sf::Color(255,255,255,255), sf::Color(255,255,255,255), "./Contents/BebasNeue-Regular.ttf", "./graphismes/logo/logo.png");
	Button Suivant((X_pos_button * 0.95f), (Y_pos_button*0.85f), 0.8f, 0.6f, menu_font_size, L"Suivant", sf::Color(255,255,255,255), sf::Color(255,255,255,255), "./Contents/BebasNeue-Regular.ttf", "./graphismes/bouton/suivant.png");
	Button Precedent((X_pos_button * 0.6f), (Y_pos_button*0.85f), 0.8f, 0.6f, menu_font_size, L"Précedent", sf::Color(255,255,255,255), sf::Color(255,255,255,255), "./Contents/BebasNeue-Regular.ttf", "./graphismes/bouton/precedent.png");

	ListButton.push_back(Menu);
	ListButton.push_back(Suivant);
	ListButton.push_back(Precedent);

	if(!getRunning())
		setAlpha(alpha_max);
	while(getRunning()){

		// Gestion des événements
		int error = processEvent(App);
		switch(error){
			case -1:
				App->close();
		    	setRunning(false);
				return -1;
			break;
			case 1:
				std::cout << "Retour au menu principal"<< std::endl;
				setRunning(false);
				setEtape(1);
				rb->reset();
				return 0;
			break;
		}

		// On mets à jour les données
		setSizeSequence(UserSequence.size());
		setSizeMotif(UserMotif.size());


		// Pour ne pas passer à étape = -1
		if(getEtape() <= 0)
			setEtape(1);

		// On augmente progressivement l'apparition des éléments
		if(getAlpha() < alpha_max)
			setAlpha(getAlpha() + 5);

		// Si la saisie est terminée
		if(getEtape() == 4 && !isRunningRb){
			isRunningRb = true;
			delete rb;
			rb = new RB(getUserSequence(), getUserMotif(), 113);
			NbOcc = rb->run();
			std::cout << "Nombre d'occurences : " << NbOcc << std::endl;
		}

		// Gestion Affichage
		App->clear();

		App->draw(S_bg);

		for(size_t i = 0; i < ListButton.size() - 1; i++){
			App->draw(ListButton[i].S_button);
		}
		if(getSizeMotif() > 0 && getSizeSequence() > 0 && getEtape() > 3)
			App->draw(ListButton[2].S_button);

		if(getEtape() < 4) Saisie(App);
		if(getEtape() > 3)
			afficheResultats(App);
		App->display();

	}

	return 0;

}

void Tester::Saisie(sf::RenderWindow *&App){
	resetContenu();
	resetTextElem();
	resetKeyElem();

	std::string s1("string");
	std::wstring ws1;			// Pour la conversion string -> wstring

	switch(getEtape()){
		case 1:
			addContenu(L"Séquence");
			addContenu(L"Veuillez saisir une séquence à analyser (20 chars max)");
		break;
		case 2:
			addContenu(L"Motif");
			addContenu(L"Veuillez saisir un motif ( <= taille de la séquence)");
		break;
		case 3:
			addContenu(L"Récapitulatif");
			addContenu(L"Voici les textes que vous avez rentré précédemment :");
			s1 = getUserSequence();
			ws1.assign(s1.begin(), s1.end());
			addContenu(L"Séquence : \t" + ws1);
			s1 = getUserMotif();
			ws1.assign(s1.begin(), s1.end());
			addContenu(L"Motif recherché : \t" + ws1);

			//std::cout << "Séquence 	: \t" << getUserSequence()  << "\tTaille : " << getSizeSequence() << std::endl;
			//std::cout << "Motif 	: \t" << getUserMotif() << "\tTaille : " << getSizeMotif() << std::endl;
		break;
		default:
		break;
	}

	// À partir des phrases au dessus, on crée des éléments sf::Text
	for(unsigned int i = 0; i < sizeContenu(); i++){
		addTextElem(sf::Text());
		this->Text_Elements[i].setFont(Font);
		this->Text_Elements[i].setColor(sf::Color(45,45,45, getAlpha()/ alpha_div));
		this->Text_Elements[i].setString(this->contenu[i]);
	}

	// AInsi que les lettres rentrées au clavier
	if(getEtape() < 3)
		for(unsigned int i = 0; i < sizeKey(); i++){
			addKeyElem(sf::Text());
			this->Key_Elements[i].setFont(Font);
			this->Key_Elements[i].setColor(sf::Color(86,100,239, getAlpha()/ alpha_div));
			this->Key_Elements[i].setString(this->pressedkey[i]);
		}
	// Initialisation du titre
	this->Text_Elements[0].setPosition(sf::Vector2f(App->getSize().x *0.45f - this->Text_Elements[0].getCharacterSize(), App->getSize().y * 0.32f - 25));
	this->Text_Elements[0].setCharacterSize(36);
	this->Text_Elements[0].setStyle(sf::Text::Bold);

	this->Text_Elements[1].setPosition(sf::Vector2f(App->getSize().x *0.25f - this->Text_Elements[0].getCharacterSize(), App->getSize().y * 0.40f - 25));
	this->Text_Elements[1].setCharacterSize(30);

	switch(getEtape()){
		case 1:
			for(size_t i = 2, deca = 25; i < sizeTextElem(); i++, deca += 100){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y * 0.60f + deca));
				this->Text_Elements[i].setCharacterSize(22);
			}

			for (size_t i = 0, deca = 56; i < sizeKeyElem(); ++i, deca += 30){
				this->Key_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.20f  + deca, App->getSize().y * 0.60f));
				this->Key_Elements[i].setCharacterSize(36);
			}
		break;
		case 2:
			for(size_t i = 2, deca = 25; i < sizeTextElem(); i++, deca += 100){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y * 0.60f + deca));
					this->Text_Elements[i].setCharacterSize(22);
				}

			for (size_t i = 0, deca = 56; i < sizeKeyElem(); ++i, deca += 30){
				this->Key_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.20f  + deca, App->getSize().y * 0.60f));
				this->Key_Elements[i].setCharacterSize(36);
			}
		break;
		case 3:
			for(size_t i = 2, deca = 25; i < sizeTextElem(); i++, deca += 100){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y * 0.45f + deca));
					this->Text_Elements[i].setCharacterSize(36);
				}
		break;
		default:
		break;
	}
	// On draw tous les éléments textuels
	for(unsigned int i = 0; i < sizeTextElem(); i++){
		App->draw(this->Text_Elements[i]);
	}
	for(unsigned int i = 0; i < sizeKeyElem(); i++){
		App->draw(this->Key_Elements[i]);
	}
}


int		Tester::processEvent(sf::RenderWindow *&App){sf::Event Event;
	while(App->pollEvent(Event)){
		// L'user a fermé la fenêtre
		if(Event.type == sf::Event::Closed){
			App->close();
			this->setRunning(false);
			return -1;
		}
		if(getEtape() == 1 || getEtape() == 2)
			if((Event.type == sf::Event::KeyPressed && sizeKey() < 20)){
				keyBind(Event);

		}

		// Si on a fait un clic de souris left
		if (Event.mouseButton.button == sf::Mouse::Left)
		{
			// Bouton Menu
			if(isButtonClicked(&ListButton[0].S_button, App)){
				// Pour éviter la persistance des données
				// on efface tout pour reprendre à l'étape 1
				// plus tard
		    	resetContenu();
		    	resetKey();
				resetTextElem();
				resetKeyElem();
				setEtape(1);
				setRunning(false);
				UserSequence = "";
				UserMotif = "";
		    	return 1;
		    	
		    }
		    if(isButtonClicked(&ListButton[1].S_button, App)){
		    	if((getEtape() == 1) && getSizeSequence() != 0){
		    		// On remet tout à zéro pour la saisie motif
		    		resetContenu();
		    		resetKey();
					resetTextElem();
					resetKeyElem();
					setEtape(getEtape() + 1);
				}else if(getEtape() == 2 && getSizeMotif() !=0 && getSizeMotif() <= getSizeSequence()){
					setEtape(getEtape() + 1);
				}else if(getEtape() > 2)
					setEtape(getEtape() + 1);
				if(getEtape() == 7){
					resetContenu();
			    	resetKey();
					resetTextElem();
					resetKeyElem();
					setEtape(1);
					setRunning(false);
					UserSequence = "";
					UserMotif = "";
			    	return 1;
				}
		    	
		    }

		    if(getEtape() > 3 && isButtonClicked(&ListButton[2].S_button, App)){
		    	setEtape(getEtape() - 1);
		    	setAlpha(0);
		    	std::cout << "Passage à l'étape " << getEtape() << std::endl;
		    }
		}

		// Si on a fait un clic de souris droit
		if (Event.mouseButton.button == sf::Mouse::Right)
		{
		    std::cout << "the right button was pressed" << std::endl;
		    std::cout << "mouse x: " << Event.mouseButton.x << std::endl;
		    std::cout << "mouse y: " << Event.mouseButton.y << std::endl;
		}

	}
	return 0;
}

void 	Tester::afficheResultats(sf::RenderWindow *&App){
	resetContenu();
	resetTextElem();

	std::string sequence = rb->getSequence();
	std::string motif = rb->getMotif();
	std::string hashVal = std::to_string(rb->getHashVal());
	std::string hashPat = std::to_string(rb->getHashPat());
	std::string hashSeq = std::to_string(rb->getHashSeq());
	std::string occ = std::to_string(rb->getOcc());
	std::vector<std::pair<std::string, std::string>> tabSeq = rb->tabSeq;
	std::vector<std::string> positions = rb->positions;

	std::string s1("string");
	std::string s2("string");
	std::wstring ws1;
	std::wstring ws2;

	size_t tmp, _tmp = 0;

	switch(getEtape()){
		case 4:
			if(getSizeMotif() < 0){
				setEtape(2);
			}else{			
				addContenu(L"Hashage");
				s1 = sequence;
				ws1.assign(s1.begin(), s1.end());
				
				addContenu(L"Séquence à analyser:\t\t" + ws1);
				s1 = motif;
				ws1.assign(s1.begin(), s1.end());
				addContenu(L"Motif à rechercher :\t\t\t" + ws1);
				
				s1 = std::to_string(rb->getCharset());
				ws1.assign(s1.begin(), s1.end());
				s2 = std::to_string(rb->getPremier());
				ws2.assign(s2.begin(), s2.end());
				addContenu(L"hash_value = (hash_value * " + ws1 + L") % " + ws2);
				s1 = hashVal;
				ws1.assign(s1.begin(), s1.end());
				addContenu(L"=> hash_value = " + ws1);
	
				s1 = std::to_string(rb->getCharset());
				ws1.assign(s1.begin(), s1.end());
				s2 = std::to_string(rb->getPremier());
				ws2.assign(s2.begin(), s2.end());
				addContenu(L"hash_motif = (hash_motif * " + ws1 + L" + motif[0..m - 1]) % " + ws2);
				s1 = std::to_string(rb->getHashPat());
				ws1.assign(s1.begin(), s1.end());
				s2 = motif;
				ws2.assign(s2.begin(), s2.end());
				addContenu(L"=> hash("+ws2+L") = " + ws1);
	
				s1 = std::to_string(rb->getCharset());
				ws1.assign(s1.begin(), s1.end());
				s2 = std::to_string(rb->getPremier());
				ws2.assign(s2.begin(), s2.end());
				addContenu(L"hash_txt = (hash_txt * " + ws1 + L" + txt[i..i + m - 1]) % " + ws2);
				s1 = rb->tabSeq[0].second;
				ws1.assign(s1.begin(), s1.end());
				s2 = rb->tabSeq[0].first;
				ws2.assign(s2.begin(), s2.end());
				addContenu(L"=> hash("+ws2+L") = " + ws1);
			}
		break;
		case 5:
			addContenu(L"Overlap");
			s1 = rb->getMotif();
			ws1.assign(s1.begin(), s1.end());
			s2 = std::to_string(rb->getHashPat());
			ws2.assign(s2.begin(), s2.end());
			addContenu(L"hash(" + ws1 + L") = " + ws2);
			for(size_t i = 0; i < tabSeq.size() - 1; i++){
				s1 = tabSeq[i].first;
				ws1.assign(s1.begin(), s1.end());
				s2 = tabSeq[i].second;
				ws2.assign(s2.begin(), s2.end());
				addContenu(L"hash(" + ws1 + L") = " + ws2);
			}
		break;
		case 6:
			if(NbOcc > 0){
				addContenu(L"Conclusion");

				for(size_t i = 0; i < sequence.length(); i++){
					s1 = sequence[i];
					ws1.assign(s1.begin(), s1.end());
					addContenu(ws1);
				}

				addContenu(L"Position des motifs dans la séquence : ");
				for(size_t i = 0; i < positions.size(); i++){
					s1 = positions[i];
					ws1.assign(s1.begin(), s1.end());
					addContenu(ws1);
				}

				s1 = occ;
				ws1.assign(s1.begin(), s1.end());
				addContenu(L"Nombre d'occurences du motif : ");
				addContenu(ws1);
			}else{
				addContenu(L"Conclusion");
				s1 = motif;
				s2 = sequence;
				ws1.assign(s1.begin(), s1.end());
				ws2.assign(s2.begin(), s2.end());
				addContenu(L"Aucune occurence de " + ws1 + L" n'a été trouvée dans\nla séquence "+ ws2 + L".");
			}
	}

	// À partir des phrases au dessus, on crée des éléments sf::Text
	for(size_t i = 0; i < sizeContenu(); i++){
		addTextElem(sf::Text());
		this->Text_Elements[i].setFont(Font);
		this->Text_Elements[i].setColor(sf::Color(45,45,45, getAlpha()/ alpha_div));
		this->Text_Elements[i].setString(this->contenu[i]);
	}

	// Initialisation du titre
	this->Text_Elements[0].setPosition(sf::Vector2f(App->getSize().x *0.45f- this->Text_Elements[0].getCharacterSize(), App->getSize().y *0.32f - 25));
	this->Text_Elements[0].setCharacterSize(36);
	this->Text_Elements[0].setStyle(sf::Text::Bold);

	// Positionnement des éléments en fonction de l'étape
	switch(getEtape()){
		case 4:
			for(size_t i = 1, deca = 25; i < this->Text_Elements.size(); i++, deca += 45){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
					if(i == 4 || i == 6 || i == 8)
						this->Text_Elements[i].setColor(sf::Color(200,45,45, getAlpha()/ alpha_div));
					this->Text_Elements[i].setCharacterSize(26);
			}
		break;
		case 5:
			// Mise en évidence de la valeur du motif
			this->Text_Elements[1].setPosition(sf::Vector2f(App->getSize().x *0.45f- this->Text_Elements[0].getCharacterSize(), App->getSize().y / 3 - 75));
			this->Text_Elements[1].setColor(sf::Color(200,45,45, getAlpha()/ alpha_div));
			this->Text_Elements[1].setCharacterSize(22);
			this->Text_Elements[1].setStyle(sf::Text::Bold);

			tmp = this->Text_Elements.size() / 2;
			if(tmp % 2 != 0) tmp++;

			// Affichage des valeurs de chaque sous-séquence:
			for(size_t i = 2, deca = 25; i < tmp; i++, deca += 30){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.35f - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
					this->Text_Elements[i].setCharacterSize(19);
			}
			for(size_t i = tmp, deca = 25; i < this->Text_Elements.size(); i++, deca += 30){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x * 0.55f - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
					this->Text_Elements[i].setCharacterSize(19);
			}
			
			for(size_t i = 0; i < tabSeq.size(); i++){
				if(tabSeq[i].first == motif){
					this->Text_Elements[i+2].setColor(sf::Color(45,200,45, getAlpha()/ alpha_div));
				}
			}
		break;
		case 6:
			if(this->NbOcc > 0){
			// On place les lettres de la séquence
			for(size_t i = 1, deca = 25; i <= sequence.length(); i++, deca += 30){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.20f - this->Text_Elements[i].getCharacterSize()  + deca, App->getSize().y * 0.4f));
					this->Text_Elements[i].setCharacterSize(28);
			}			

			
				// Marquage en vert des sous-séquence qui sont égales au motif
				for (size_t i = 1; i <= sequence.length(); ++i)
				{	
					for(size_t j = 0; j <= positions.size(); j++){
						if(positions[j] == std::to_string(i - 1)){
							for(size_t k = i; k < i + getSizeMotif(); k++)
							this->Text_Elements[k].setColor(sf::Color(45,200,45, getAlpha()/ alpha_div));
						}
						tmp = i;
					}
				}

				// Position des motifs dans la séquence
				this->Text_Elements[tmp + 1].setPosition(sf::Vector2f(App->getSize().x *0.25f - this->Text_Elements[tmp].getCharacterSize()  + 25, App->getSize().y * 0.5f));
				this->Text_Elements[tmp + 1].setCharacterSize(26);
				this->Text_Elements[tmp + 1].setColor(sf::Color(86,216,239, getAlpha()/ alpha_div));
				
					for(size_t i = tmp + 2, deca = 25; i < tmp + 2 + positions.size(); i++, deca+= 40){
						this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.2f - this->Text_Elements[i].getCharacterSize()  + deca, App->getSize().y * 0.60f));
						this->Text_Elements[i].setCharacterSize(26);
						this->Text_Elements[i].setColor(sf::Color(45,200,45, getAlpha()/ alpha_div));
						_tmp = i + 1;
					}
				

				// Nombre d'occurences
				this->Text_Elements[_tmp].setPosition(sf::Vector2f(App->getSize().x *0.25f - this->Text_Elements[_tmp].getCharacterSize() + 25, App->getSize().y * 0.65f));
				this->Text_Elements[_tmp].setCharacterSize(26);
				this->Text_Elements[_tmp].setColor(sf::Color(86,216,239, getAlpha()/ alpha_div));

				this->Text_Elements[_tmp + 1].setPosition(sf::Vector2f(App->getSize().x *0.6f - this->Text_Elements[_tmp].getCharacterSize() + 25, App->getSize().y * 0.65f));
				this->Text_Elements[_tmp + 1].setCharacterSize(30);
				this->Text_Elements[_tmp + 1].setColor(sf::Color(45,200,45, getAlpha()/ alpha_div));
				
			}else{
				this->Text_Elements[1].setPosition(sf::Vector2f(App->getSize().x *0.2f- this->Text_Elements[0].getCharacterSize(), App->getSize().y *0.45f));
				this->Text_Elements[1].setCharacterSize(26);
				this->Text_Elements[1].setColor(sf::Color(230,50,85, getAlpha()/ alpha_div));

			}
		break;
	}

	// On draw tous les éléments textuels à l'écran
	for(unsigned int i = 0; i < sizeTextElem(); i++){
		App->draw(this->Text_Elements[i]);
	}
}



void	Tester::addContenu(std::wstring c){
	this->contenu.push_back(c);
}

void	Tester::setContenu(int index, std::wstring newContenu){
	this->contenu.at(index) = newContenu;
}

void	Tester::rmContenu(int index){
	this->contenu[index].erase();
}

unsigned int	Tester::sizeContenu()const{
	return this->contenu.size();
}

void	Tester::resetContenu(){
	this->contenu.clear();
}

void	Tester::addTextElem(sf::Text t){
	this->Text_Elements.push_back(t);
}

void	Tester::setTextElem(int index, sf::Text t){
	this->Text_Elements.at(index) = t;
}

void	Tester::resetTextElem(){
	this->Text_Elements.clear();
}

unsigned int	Tester::sizeTextElem()const{
	return this->Text_Elements.size();
}

std::string	Tester::getUserSequence() const{
	return this->UserSequence;
}
void Tester::setUserSequence(std::string us){
	this->UserSequence = us;
}

std::string	Tester::getUserMotif() const{
	return this->UserMotif;
}
void Tester::setUserMotif(std::string um){
	this->UserMotif = um;
}

int Tester::getSizeSequence()const{
	return this->sizeUserSequence;
}
void Tester::setSizeSequence(const int size){
	this->sizeUserSequence = size;
}

int Tester::getSizeMotif()const{
	return this->sizeUserMotif;
}
void Tester::setSizeMotif(const int size){
	this->sizeUserMotif = size;
}

void	Tester::addKey(std::wstring c){
	this->pressedkey.push_back(c);
}
void	Tester::resetKey(){
	this->pressedkey.clear();
}
unsigned int	Tester::sizeKey()const{
	return this->pressedkey.size();
}

void 	Tester::delKey(){
	this->pressedkey.pop_back();
}

void	Tester::addKeyElem(sf::Text t){
	this->Key_Elements.push_back(t);
}

void	Tester::setKeyElem(int index, sf::Text t){
	this->Key_Elements.at(index) = t;
}

void	Tester::resetKeyElem(){
	this->Key_Elements.clear();
}

unsigned int	Tester::sizeKeyElem()const{
	return this->Key_Elements.size();
}

void Tester::keyBind(sf::Event Event){
	switch(Event.key.code){
    	case(sf::Keyboard::A):
	    	addKey(L"A");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("A");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("A");
	    break;
	    case(sf::Keyboard::B):
	    	addKey(L"B");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("B");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("B");
	    break;
	    case(sf::Keyboard::C):
	    	addKey(L"C");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("C");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("C");
	    break;
	    case(sf::Keyboard::D):
	    	addKey(L"D");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("D");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("D");
	    break;
	    case(sf::Keyboard::E):
	    	addKey(L"E");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("E");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("E");
	    break;
	    case(sf::Keyboard::F):
	    	addKey(L"F");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("F");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("F");
	    break;
	    case(sf::Keyboard::G):
	    	addKey(L"G");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("G");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("G");
	    break;
	    case(sf::Keyboard::H):
	    	addKey(L"H");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("H");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("H");
	    break;
	    case(sf::Keyboard::I):
	    	addKey(L"I");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("I");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("I");
	    break;
	    case(sf::Keyboard::J):
	    	addKey(L"J");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("J");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("J");
	    break;
	    case(sf::Keyboard::K):
	    	addKey(L"K");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("K");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("K");
	    break;
	    case(sf::Keyboard::L):
	    	addKey(L"L");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("L");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("L");
	    break;
	    case(sf::Keyboard::M):
	    	addKey(L"M");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("M");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("M");
	    break;
	    case(sf::Keyboard::N):
	    	addKey(L"N");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("N");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("N");
	    break;
	    case(sf::Keyboard::O):
	    	addKey(L"O");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("O");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("O");
	    break;
	    case(sf::Keyboard::P):
	    	addKey(L"P");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("P");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("P");
	    break;
	    case(sf::Keyboard::Q):
	    	addKey(L"Q");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("Q");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("Q");
	    break;
	    case(sf::Keyboard::R):
	    	addKey(L"R");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("R");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("R");
	    break;
	    case(sf::Keyboard::S):
	    	addKey(L"S");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("S");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("S");
	    break;
	    case(sf::Keyboard::T):
	    	addKey(L"T");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("T");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("T");
	    break;
	    case(sf::Keyboard::U):
	    	addKey(L"U");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("U");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("U");
	    break;
	    case(sf::Keyboard::V):
	    	addKey(L"V");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("V");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("V");
	    break;
	    case(sf::Keyboard::W):
	    	addKey(L"W");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("W");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("W");
	    break;
	    case(sf::Keyboard::X):
	    	addKey(L"X");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("X");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("X");
	    break;
	    case(sf::Keyboard::Y):
	    	addKey(L"Y");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("Y");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("Y");
	    break;
	    case(sf::Keyboard::Z):
	    	addKey(L"Z");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("Z");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("Z");
	    break;
	    case(sf::Keyboard::Num0):
	    	addKey(L"0");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("0");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("0");
	    break;
	    case(sf::Keyboard::Num1):
	    	addKey(L"1");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("1");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("1");
	    break;
	    case(sf::Keyboard::Num2):
	    	addKey(L"2");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("2");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("2");
	    break;
	    case(sf::Keyboard::Num3):
	    	addKey(L"3");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("3");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("3");
	    break;
	    case(sf::Keyboard::Num4):
	    	addKey(L"4");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("4");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("4");
	    break;
	    case(sf::Keyboard::Num5):
	    	addKey(L"5");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("5");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("5");
	    break;
	    case(sf::Keyboard::Num6):
	    	addKey(L"6");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("6");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("6");
	    break;
	    case(sf::Keyboard::Num7):
	    	addKey(L"7");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("7");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("7");
	    break;
	    case(sf::Keyboard::Num8):
	    	addKey(L"8");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("8");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("8");
	    break;
	    case(sf::Keyboard::Num9):
	    	addKey(L"9");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("9");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("9");
	    break;
	    case(sf::Keyboard::Numpad0):
	    	addKey(L"0");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("0");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("0");
	    break;
	    case(sf::Keyboard::Numpad1):
	    	addKey(L"1");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("1");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("1");
	    break;
	    case(sf::Keyboard::Numpad2):
	    	addKey(L"2");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("2");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("2");
	    break;
	    case(sf::Keyboard::Numpad3):
	    	addKey(L"3");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("3");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("3");
	    break;
	    case(sf::Keyboard::Numpad4):
	    	addKey(L"4");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("4");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("4");
	    break;
	    case(sf::Keyboard::Numpad5):
	    	addKey(L"5");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("5");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("5");
	    break;
	    case(sf::Keyboard::Numpad6):
	    	addKey(L"6");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("6");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("6");
	    break;
	    case(sf::Keyboard::Numpad7):
	    	addKey(L"7");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("7");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("7");
	    break;
	    case(sf::Keyboard::Numpad8):
	    	addKey(L"8");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("8");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("8");
	    break;
	    case(sf::Keyboard::Numpad9):
	    	addKey(L"9");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("9");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("9");
	    break;
	    case(sf::Keyboard::F1):
	    	addKey(L"1");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("1");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("1");
	    break;
	    case(sf::Keyboard::F2):
	    	addKey(L"2");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("2");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("2");
	    break;
	    case(sf::Keyboard::F3):
	    	addKey(L"3");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("3");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("3");
	    break;
	    case(sf::Keyboard::F4):
	    	addKey(L"4");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("4");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("4");
	    break;
	    case(sf::Keyboard::F5):
	    	addKey(L"5");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("5");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("5");
	    break;
	    case(sf::Keyboard::F6):
	    	addKey(L"6");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("6");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("6");
	    break;
	    case(sf::Keyboard::F7):
	    	addKey(L"7");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("7");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("7");
	    break;
	    case(sf::Keyboard::F8):
	    	addKey(L"8");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("8");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("8");
	    break;
	    case(sf::Keyboard::F9):
	    	addKey(L"9");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("9");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("9");
	    break;
	    case(sf::Keyboard::F10):
	    	addKey(L"0");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("0");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("0");
	    break;
	    case(sf::Keyboard::F11):
	    	addKey(L"1");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("1");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("1");
	    break;
	    case(sf::Keyboard::F12):
	    	addKey(L"2");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("2");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("2");
	    break;
	    case(sf::Keyboard::F13):
	    	addKey(L"3");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("3");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("3");
	    break;
	    case(sf::Keyboard::F14):
	    	addKey(L"4");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("4");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("4");
	    break;
	    case(sf::Keyboard::F15):
	    	addKey(L"5");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("5");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("5");
	    break;
	    case(sf::Keyboard::Space):
	    	addKey(L" ");
	    	if(getEtape() == 1)
	    		this->UserSequence.append(" ");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append(" ");
	    break;
	    case(sf::Keyboard::Quote):
	    	addKey(L"'");
	    	if(getEtape() == 1)
	    		this->UserSequence.append("'");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append("'");
	    	break;
	    case(sf::Keyboard::BackSpace):
	    	
	    	if(getEtape() == 1 && UserSequence.length() > 0){
	    		delKey();
	    		this->UserSequence.pop_back();
	    	}
	    	else if(getEtape() == 2 && UserMotif.length() > 0){
	    		delKey();
	    		this->UserMotif.pop_back();
	    	}
	    break;
	    case(sf::Keyboard::SemiColon):
	    	addKey(L";");
	    	if(getEtape() == 1)
	    		this->UserSequence.append(";");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append(";");
	    break;
	    case(sf::Keyboard::Comma):
	    	addKey(L",");
	    	if(getEtape() == 1)
	    		this->UserSequence.append(",");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append(",");
	    break;
	    case(sf::Keyboard::Period):
	    	addKey(L".");
	    	if(getEtape() == 1)
	    		this->UserSequence.append(".");
	    	else if(getEtape() == 2)
	    		this->UserMotif.append(".");
	    break;
	    default:
	    	break;
	}
}
