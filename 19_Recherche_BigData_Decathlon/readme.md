# Décathlon

## Résumé

C’est dans le cadre de l’exploitation et l’application de nos connaissances acquises dans l’UE HMIN122M Entrepôt de Données et Big Data que la réalisation de ce mini-projet, sur l’enseigne française Décathlon, s'incrit.

## Notions abordées

Développement d’un Data Warehouse fictif
pour l’entreprise Décathlon : 

* analyse des besoins, des opérations importantes de l'entreprise et des requêtes potentiellement demandées par un responsable de direction utilisant un outil d'aide à la décision ;
* conception de l'entrepôt de données, choix des opérations importantes, conception des data-marts et des dimensions et évaluation de notre modèle en fonction des besoins ;
* Implémentation en SQL ORACLE, créations de vues virtuelles/ matérialisées et traitement des requêtes analytiques

## En savoir plus

Allez dans le dossier "MiniProjet-Décathlon" et ouvrez le rapport qui porte le nom "EDBD_rapportMiniProjet_TRIBOUT_PLUTA_BESSON" ainsi que la diapositive associée "Presentation_orale.pdf"