ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_jour CHECK (jour BETWEEN 0 AND 31);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_jourSemaine CHECK (jourSemaine BETWEEN 1 AND 7);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_jourAnnee CHECK (jourAnnee BETWEEN 1 AND 366);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_semaine CHECK (semaine BETWEEN 1 AND 53);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_semaineDuMois CHECK (semaineDuMois BETWEEN 1 AND 5);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_mois CHECK (mois BETWEEN 1 AND 12);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_trimestre CHECK (trimestre BETWEEN 1 AND 4);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_annee CHECK (annee >= 1976);

ALTER TABLE CurrentDate ADD CONSTRAINT CHECK_vacances CHECK (vacances BETWEEN 0 AND 1);

ALTER TABLE Temps ADD CONSTRAINT CHECK_heure CHECK (heure BETWEEN 0 AND 12);

ALTER TABLE Temps ADD CONSTRAINT CHECK_minute CHECK (minute BETWEEN 0 AND 60);

ALTER TABLE Temps ADD CONSTRAINT CHECK_seconde CHECK (seconde BETWEEN 0 AND 60);

ALTER TABLE Temps ADD CONSTRAINT CHECK_fuseauHoraire CHECK (fuseauHoraire IN ("AM", "PM"));

ALTER TABLE Temps ADD CONSTRAINT CHECK_momentJourneee CHECK (momentJournee IN ("Matin", "Midi", "Apres-Midi", "Soir"));


ALTER TABLE Produit ADD CONSTRAINT CHECK_prixHT CHECK (prixHT >= 0.0);

ALTER TABLE Produit ADD CONSTRAINT CHECK_prixAchatFournisseur CHECK (prixAchatFournisseur >= 0.0);

ALTER TABLE Produit ADD CONSTRAINT CHECK_tauxTaxe CHECK (tauxTaxe BETWEEN 0 AND 99);

ALTER TABLE Produit ADD CONSTRAINT CHECK_prixTTC CHECK (prixTTC >= 0.0);

ALTER TABLE Produit ADD CONSTRAINT CHECK_marge CHECK (marge >= 0.0);

ALTER TABLE Stock ADD CONSTRAINT CHECK_quantiteEnRayon CHECK (quantiteEnRayon >= 0);

ALTER TABLE Stock ADD CONSTRAINT CHECK_quantiteEntrepot CHECK (quantiteEntrepot >= 0);

ALTER TABLE Stock ADD CONSTRAINT CHECK_quantiteTotale CHECK (quantiteTotale >= 0);

ALTER TABLE Vente ADD CONSTRAINT CHECK_prixVente CHECK (prixVente >= 0.0);

	