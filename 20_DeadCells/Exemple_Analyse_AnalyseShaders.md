# Analyse de l'impact des shaders sur les performances du jeu

## Appareils de test

* Sony Xperia 10 (1080 x 2520) muni d'un GPU Adreno 509, architecture **arm64-v8a**
* Samsung A10 (720 x 1520) muni d'un GPU Mali-G71 MP2, architecture armeabi--v7
* Huawei MediaPad T5 (1920 x 1200) muni d'un GPU Mali-T830 MP2

## Process de test

* Pour chaque Test ID:
  * Exécuter un test de 5 secondes (4-5 fois et prendre le plus pertinent (stable))
    * Capture d'écran du tableau d'occurence
    * Capture d'écran de l'histogramme chronologique
    * Reporter les valeurs dans le tableau de l'appareil
  * Exécuter le benchmark pour 1800 frames :
    * Capture d'écran du rapport de benchmark
    * Reporter les valeurs dans le tableau de l'appareil

Pour chaque appareil, chaque test doit être effectué dans les mêmes conditions de jeu.

Les tests qui concernent la réflexion doivent être effectués dans un niveau disposant de suffisament d'effets de réflexion (Pont Noir, la Tour-Horloge).

### Cas du Discard

**L'opération `discard` est un processus possible uniquement au niveau des fragment shaders, lui indiquant de ne pas écrire le fragment dans l'output.** 

Généralement, il est observé une diminution des performances d'environs 25% selon plusieurs témoignages [<a href="#2">2</a>] et se voit être déconseillé d'utilisation [<a href="#3">3</a>] . Cela serait principalement dû au fait qu'un fragment qui est `discard`implique que le GPU doit générer de nouveau **Depth Test** [<a href="#6">6</a>] et un **Depth Write** [<a href="#1">1</a>], ce dernier ne pouvant plus attester de la "vraie" profondeur du fragment. En effet, la majorité des GPU executent un **Early Fragment Test** pour prévenir de l'execution non nécessaire du fragment shader basé sur un **Depth Test**. En fonction des architectures des GPU et de leurs optimisations internes (parallélisation, hierarchisation, ...), un `discard`peut avoir un impact notable et particulièrement sur les architectures mobiles.

Une piste, souvent mise en avant pour le développement d'application graphiques sur mobile, serait d'activer le blending des surfaces transparentes plutôt que de `discard`. [<a href="#4">4</a>]

### Cas du LightAccu

La gestion de l'éclairage par de multiples sources de lumière, impliquant l'addition de la lumière ambiante avec la reflection diffuse et speculaire sur un material par le fragment shader, est une source possible de frame-dropping.

### Cas de la Reflexion

La reflexion implique que des objets soient rendu de multiples fois en fonction des zones réfléxives, surchargeant ainsi les calculs. 

## Observations sur le test "Frametimes"

### Données enregistrées

On enregistre la valeur minimale, médiane, maximale, moyenne donnés en millisecondes ainsi que la variance (dispertion des valeurs autours de la moyenne) pour chaque Test ID. On estime les FPS moyens tels que ~FPS = 1 / (AVG * 0,001).

On fixe une valeur d'étallonage (Calibrated value) de FPS correspondant au cas où tous les shaders sont activés (Test ID A2 et A2R) permettant de calculer la différence relative des autres cas de tests (Δ FPS).

### Interprétations

La désinclusions des instructions comportant un `discard` du code augmente sensiblement les performances entre 2 et 2,5 FPS et ce quelque soit l'appareil. Il en va de même pour les instructions qui concernent le Light Accumulating.

En désactivant les opération `glow` et `glowLscat`, et ce quelque soit l'appareil, on a un gain allant de 7,5 pour le Huawei à 17 FPS pour le Samsung. On peut donc considérer cette opération comme très lourde et ayant énormément d'impacts sur les performances.

La désactivation de la réflexion augmente considérablement les gains de performances, bien qu'un peu moins marqué sur le Sony. Possible que ça soit lié à la différence d'architecture avec les deux autres appareils.

## Ressources

* <a id="#1" href=https://fgiesen.wordpress.com/2011/07/09/a-trip-through-the-graphics-pipeline-2011-index/> A trip through the graphic's pipeline - The rig blog </a>
* <a id="#2" href="https://www.gamedev.net/forums/topic/655602-why-discard-pixel-take-a-noticeable-performance-hit/"> Why discard pixel take a noticeable performance hit ? </a>
* <a id="#3" href=https://en.wikibooks.org/wiki/GLSL_Programming/GLUT/Cutaways>GLSL Programming/GLUT/Cutaways - Wikibooks </a>
* <a id="#4" href=https://docs.imgtec.com/PowerVR_Architecture/topics/rules/c_GoldenRules_do_not_use_discard.html>Do not use discard - PowerVR Developer Documentation </a>
* <a id="#5" href="https://gamedev.stackexchange.com/questions/40301/do-i-lose-gain-performance-for-discarding-pixels-even-if-i-dont-use-depth-testi">Do I lose/gain performance for discarding pixels</a>
* <a id="#6" href="https://www.khronos.org/opengl/wiki/Early_Fragment_Test">Early Fragment Test - Khronos</a>
* Optimisation de Shaders pour supports mobiles - Google Sheets de Cédric PLUTA </a>