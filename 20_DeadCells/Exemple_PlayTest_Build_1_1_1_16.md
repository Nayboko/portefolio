# Review Build 1.1.1i(16) - Optim

## Pre-test de la build sur Samsung A10 en Idle au start

Après avoir désactivé des éléments de passes de shaders lié au glowLscat, on obtient un petit gain de performance :

* \~4,5 FPS en scénario Idle (aucun mouvement du personnage) sur Samsung A10 (Mali G71)

=> Mesures de références = Min : 16 ms - Med : 22 ms - Max : 33 ms - Avg : 22 => \~45,5 FPS

=> Avec nos modifications = Min : 18 ms - Med : 20 ms - Max : 23 ms - Avg : 20 => \~50 FPS

Testé sur un Samsung A10 (GPU Mali) sans aucune autre modification.

Visuellement, alors qu'une désactivation totale de la passe fait disparaître tous les éléments qui ne sont pas du décors, nos désactivations permettent de les conserver mais il n'y a plus d'effet de glowing.

## Test interne build v 1.1.1i (16)-optim

Run classique sur version android-release (sans debug frame)
[Tableau des devices personnels]()
[Mesures de performance en fonction des pass shaders utilisées]()

### Cédric

Alors pour le Redmi Note 7 (Adreno 512) :

- Promenade des condamnés (lvl 2) a parfois des framedrops, certainement dû au très grand nombre d’éléments (décors, ennemies, effets)
- Black Bridge : *crash Missing global value reflect.depth for shader (not found)*
- En dehors de ça, fluide et stable dans l’ensemble mais pas de review possible sur les niveaux à reflexion

Pour la MediaPad M5 (Mali T830):

- Promenade des condamnés (lvl 2)  et une partie du lvl 3 pas fluide, raison similaires à celles citées ci-dessus
- Si nb ennemis tués >= 2, framedroping important
- Menu déroulant de la mutation a un comportement déroutant (remonte, descend tout seul) ce qui gène la selection des mutations.
- Pas d’artéfacts graphiques détectés

Pour la Lenovo Tab3 730F (Mali T720 MP2) :

* Musique se coupe régulièrement et ce dès l'écran principal ; certainement du à l'instabilité des FPS
* Ghosting du personnage principal (désactiver postProcessing ?)
* Artefacts visuels :
  * "glitchs" multi-colors à l'inter-level
  * carrés bleus donnant un effet "mozaïque" au niveau du sol du level 2 (Promenade des condamnés)
* Pas fluide de bout en bout, à la limite du jouable par moment (\~15FPS voir moins)

Vidéo : _

Pour Samsung A10 (Mali G71 MP2) :

* Fluide de bout en bout, très peu de framedrop même avec plusieurs ennemis et lors des combats
* Pas d'artefacts

Vidéo : _

### _

**Appareil :** Xiaomi Mi 9, Adreno 640

Pas de lag du tout, d'une fluidité folle 

### _

**Appareil :** Pixel 3, Adreno 630

Nickel

Crash au moment d'arriver sur Black Bridge (et rencontré le concierge) (Voir screenshoot du crash sur Redmi Note 7 de Cédric plus haut)

Effectivement, les ennemis ça se spot pas mal surtout sur le 2nd level qu'il manque du glow mais ça passe

### _

**Appareil :** Xiaomi Mi 9, Adreno 640

Même erreur/crash que Laura, et j'ai eu une déconnexion de la manette à un moment, mais possible que ce soit une mauvaise manip de ma part ou le BT qui part en couille

Sinon tout bon et très fluide comme l'as dit Antonin vu qu'on a le même tél

### _

**Appareil :** Samsung Galaxy A5 2017

Ça ralentit un peu quand il commence à avoir pas mal de trucs à l'écran (là, je m'étais pris un gros coup du mob squelette de la promenade + y'avait un zombie + j'avais une tourelle en place)

### _

**Appareils :** 

* LG K10 2017, Mali T860 MP2
* Lenovo TB3-X70F, Mali T720

Alors j'ai un peu tester sur mon smartphone LG K10 2017 et ça marche plutôt bien mais je vous donnerais plus de détails quand j'aurais plus testé

Vidéo sur LG: _

Par contre sur ma tabletas Lenovo Tab3 10 Plus (qui n'est, je ne vous le cache pas, pas une foudre de guerre) c'est... disons intéressant

_image_
_image_

Vidéo Tab3 : _