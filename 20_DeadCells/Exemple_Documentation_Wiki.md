# Document d'exemple de documentation

Au cours de mon travail sur Dead Cells, j'ai été amené à me confronter avec de nombreuses technologies et à apporter des solutions pour mon équipe, que ce soit par la réalisation d'**outils d'automatisation** ou bien rédiger des **documents support** pour mes collègues.


# Astuce Android Studio (Extrait)

## Cohabitation des modules non-Gradle Java et Android-Gradle

```
Unsupported Modules Detected: Compilation is not supported for following modules: MyApp. Unfortunately you can't have non-Gradle Java modules and Android-Gradle modules in one project.
```

### **Solution**

1. Fermer le projet
2. Fermer entièrement l'IDE Android Studio
3. Supprimer le répertoire `.idea` du projet courant
4. Supprimer tout les fichiers `.iml` du projet courant
5. Ouvrir de nouveau Android Studio et importer le projet

### **Astuces**

* Pour activer l'affichage des fichiers cachés sur MacOS, pressez `Command + Shift + point`

# Astuce Profilers pour Android (Extrait)
## Avant-propos

Vous trouverez ici des outils de profiling GPU qui m'ont été conseillé et que j'ai pu également trouver. Dans l’absolu, les profilers GPU specifics permettent d’avoir plus d’information que GAPID (ex: ms par passe de shaders) ou tout autre profiler multi-GPU. Pour autant, RenderDoc a l’air d’être une bonne piste et supporte plus de devices que ARM Graphics Analyzer.

***

## Liste de profilers

* [[1](#arm-graphic-analyzer)] ARM Graphics Analyzer : [http://developer.arm.com/](http://developer.arm.com/) [Supported Devices](https://developer.arm.com/tools-and-software/graphics-and-gaming/arm-mobile-studio/support/supported-devices) 
* RenderDoc : [https://renderdoc.org/](https://renderdoc.org/) (Version MacOS nightly)
* Snapdragon Profiler (pour Adreno 4xx et + aussi) : [Snapdragon GPU Profiler](https://developer.qualcomm.com/software/snapdragon-profiler)
* Adreno profiler (2xx ou 3xx) : [Adreno GPU Profiler](https://developer.qualcomm.com/software/adreno-gpu-profiler)
* APITrace : [Dépôt Git ApiTrace](https://github.com/apitrace/apitrace/blob/master/docs/INSTALL.markdown)  
* ~~OpenGL Debugger : [Lien vers gDEBugger](https://www.opengl.org/sdk/tools/gDEBugger/)~~ (No longer supported)
* ~~Mali Profiler : [Mali GPU Profiler](https://developer.samsung.com/galaxy-gamedev/arm-mali.html)~~ (Remplacé par ARM Graphics Analyzer)

## Installation et configuration
Les profilers qui seront décrit dans les lignes suivante ont été installé sous un système MacOS. Les procédures sont assez analogues sur des systèmes UNIX ou Linux. Les configurations des logiciels sont eux semblables sur toutes les plateformes.

### Le device 

Assurez vous que le **Developer Mode** de votre device soit activé et que le **USB Debugging** le soit également (Settings > Developer options).
Vous pouvez vérifier que votre device est bien connecté à votre système avec la commande `adb devices` vous permettant également de récupérer l'identifiant de votre device.

### ARM Graphic Analyzer

**ATTENTION** : Tous les devices ne sont pas éligibles à la possibilité d'exécuter une trace avec ce profiler. 

Veuillez vous référer à [la liste des devices supportés](https://developer.arm.com/tools-and-software/graphics-and-gaming/arm-mobile-studio/support/supported-devices).

#### Configuration du poste

1. Installez Python 3.x sur votre poste
```sh
brew install python
```
**Note:** S'il vous arrive d'avoir des problèmes de droits d'écritures sur les dossiers `usr/local`, vous pouvez récupérer la propriété avec la commande suivante : 
```sh
sudo chown -R $(whoami) $(brew --prefix)/*
```

2. Si ce n'est pas déjà fait, installez [Android SDK platform tools](https://developer.android.com/studio/releases/platform-tools.html). Il doit vous permettre d'obtenir l'outil ADB (Android Debug Bridge).

3. Editez votre variable d'environnement `PATH` pour ajouter le répertoire d'Android SDK platform tools.
```sh
export PATH="/Users/MyName/Library/Android/sdk/platform-tools:$PATH"
```

4. Vérifiez que le port `5002`de votre machine (`127.0.0.1`) est libre. Pour vous en assurer, vous pouvez exécuter la commande suivante : 
```sh
lsof -nP +c 15 | grep LISTEN
```
Vous pourrez ainsi voir tous les ports actuellement en écoute et actifs sur votre système.
### Configuration de ARM Graphics Analyzer
1. Installez ARM Mobile Studio et démarrer ARM Graphics Analyzer

2. Avec un terminal, rendez-vous dans le répertoire d'installation d'ARM Mobile Studio pour trouver le script python `aga_me.py` : 
```sh
cd <installDirectory>/graphics_analyzer/target/android/arm
```

3. Listez les packages **debuggables** actuellement disponibles sur votre device en exécutant le script `aga_me.py` tel que : 
```sh
python3 aga_me.py --list_packages
````
**Note:** Un seul device doit être connecté à la fois sur votre système. De même pour être repéré, votre application doit être en cours d'exécution.

4. Pour les applications OpenGL ES, exécutez le script et lisez attentivement les instructions: 
```sh
python3 aga_me.py com.DefaultCompanyName.AppName
```

5. Sur ARM Graphics Analyzer, ouvrez le Device Manager depuis le menu Debug. Si votre device est bien connecté à votre système, il apparaîtra dans la liste des appareils Android.
    * IP Address : 127.0.0.1
    * Port : 5002
    * Cliquez sur **Connect**

6. Sur le terminal, presser la touche Entrée pour continuer le script. L'application va se relancer et il la trace s'exécutera en temps réel sur ARM Graphics Analyzer. Pressez de nouveau Entrée si vous souhaitez stopper la trace.

# Astuce Android App Bundle (Extrait)

Un petit point sur AAB (Android App Bundle) qui vise à remplacer les APK à terme.

## Documentation : 
* [About Android App Bundles - developer.android.com](https://developer.android.com/guide/app-bundle/)
* [Sign your app - developer.android.com](https://developer.android.com/studio/publish/app-signing.html#sign_release)
* [Android App Bundles: Getting Started](https://www.raywenderlich.com/9043-android-app-bundles-getting-started)
* [Top 7 Takeaways for Android App Bundles](https://www.youtube.com/watch?v=st9VZuJNIbw)

## C’est quoi  : 
AAB est un format pour soumettre une app sur les serveurs google de manière à ce que des split-APK soient générés directement sur serveur en fonction de la config requise par l’utilisateur. Les APK générés sont plus petits, plus spécifiques et adaptés au device de l’utilisateur.

## Comment on fait  : 
La doc est relativement claire sur le sujet, j’ai pas rencontré de problèmes majeurs pour migrer vers AAB. Le keystore (ensemble de clefs de signature) utilisé jusque là pour signer manuellement l’APK doit être encrypté et uploadé sur les serveurs google. De cette manière, lorsque Google génère les APK, il les signe en utilisant la clef fournie.

De la même manière, les AAB générés sur nos machines doivent être signés avec un autre keystore  d’import (de préférence différent de celui de signature), à partir duquel est généré un certificat. En fournissant le certificat aux serveurs de Google, ceux-ci identifient que l’AAB a été signé par nous, et en déduit la clef de signature à utiliser au moment de la génération des APK.

Ainsi la clef de signature peut n’exister que sur les serveurs google, et de notre côté on maintient uniquement une clef d’importation/upload. Contrairement à la clef de signature, une clef d’importation peut-être modifiée et/ou remplacée si elle est compromise sans obliger à changer l’appID et tout le toutim.
Reste à voir en production si les AAB sont exploitables avec la QA ou des builds standalone.

## Déployer sur un device connecté à partir d'un Android App Bundle

### Mise en oeuvre
Afin de tester la mise en place du AAB ainsi que les éventuelles integrations dynamiques de modules, il faut éditer la configuration du jeu / application afin que le déploiement (“Deploy”) soit en _APK from app bundle_ au lieu de _Default APK_ .

* Sélectionner Run > Edit Configurations [...]