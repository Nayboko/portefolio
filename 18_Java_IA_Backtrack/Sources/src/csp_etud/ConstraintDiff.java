package csp_etud;

import java.io.BufferedReader;

public class ConstraintDiff extends Constraint {

	public ConstraintDiff(BufferedReader in) throws Exception{
		super(in);
	}
	
	@Override
	public boolean violation(Assignment a) {
		for(int i = 0; i < this.varList.size(); i++) {
			if(!(a.containsKey(this.varList.get(i)))) return false;
			//if(!autorized(a)) return true;
			// Si val de a est egal a  la contrainte alors violation
		}
		for(int i = 0; i < this.varList.size(); i++){
			for(int j = i + 1; j < this.varList.size(); j++){
				if(a.get(this.varList.get(i)).equals(a.get(this.varList.get(j)))){
					return true;
				}
			}
		}
		return false;
	}
	
	/*public boolean autorized(Assignment a) {
		for(int i = 0; i < this.varList.size(); i++) {
			if(!a.get(this.varList.get(i)).equals(a.get(this.varList.get(i +1 )))) return false;
		}
		return true;
	}*/
	
	public String toString() {
		return "\n\t Differents : "+ super.toString(); 
	}
}
