# Feuille de route du Rapport de projet

## 1. Synopsis

Rapport final du projet de fin d'étude de **Licence 3 mention Informatique** de l'_Université de Montpellier_, en collaboration avec le _LIRMM_ et le _Département Informatique de la Faculté des Sciences_. Le travail est centré sur l'élaboration de scénarios d'assistance aux personnes âgées à l'aide du robot Nao de la société Aldebarran/SoftBank et d'autres objets connectés.

## 2. Les plans de construction du rapport de projet 
### A. Plan initial du Jeudi 20 Avril 2017
Voici le **plan général** pour le rapport de projet sur _Nao et les objets connectés_. Il prend en considération les différentes facettes de ce dernier, permettant une lecture fluide et cohérente, et autorise l'ajout de nouvelles sous-rubriques pour affiner le rapport.

---

#### I/ Introduction
Description générale du déroulement du projet, du cadre dans lequel il est réalisé, les objectifs de l'unité d'enseignement et une brève description du plan. Cette partie contiendra également les remerciements.

#### II/ Etat de l'art
Après avoir introduit l'objet principal du projet de recherche, on passe en revue ce qui a d'ores et déjà été réalisé dans le secteur public et privé. L'objectif est de dégager les idées principales de chaque projet, de peser le pour et le contre et de mettre en exergue ceux qui sont les plus notables. 

Puis, on introduit une idée générale, l'utilisation possible du même concept central avec des éléments nouveaux.

#### III/ L'Internet des objets
Cette partie est consacrée à une revue des différentes technologies rencontrées durant le travail de recherche, principalement pendant la veille technologique et informationnelle.

#### IV/ Notre solution
On va donner explicitement la définition client, c'est à dire la commande faite par le client concernant ce projet, puis les spécifications du livrable, où nous donnerons ce qui a été définit au début du projet, ce qui est donc attendu à la fin de celui-ci. 

Une partie importante est consacrée à l'élaboration du projet et à sa mise en oeuvre, à savoir une rapide description des différentes phases de travail, sa répartition entre les membres (éventuellement un diagramme de gantt), puis une explication plus approfondie de chaque élément constituant le projet.

Une sous-section sera également consacrée à la présentation d'un point technique et central : 
* Ce qui a été retenu pour son implémentation
* Comment nous l'avons implémenté et quelles techniques ont été utilisées
* Ce qui a posé problème
* Perspectives, ce qui pourrait être amélioré


#### V/ Bug Report
Les possibilités d'amélioration, on répertorie les éventuels bugs dans les programmes, et les solutions qui nous semblent concevables.

#### VI/ Conclusion
Si on devait refaire le projet, quelles sont les points que nous changerions dans son déroulement, les choses qui pourraient être améliorée, en somme, prendre du recul sur ce qui a été réalisé avant d'émettre des perspectives.

#### Bibliogaphie / Webographie
Ensemble de la documentation qui a permit à chaque membre du groupe d'avancer dans son travail, d'améliorer, de consolider ou d'acquérir de nouvelles compétences. Le lecteur pourra, s'il en ressent l'envie, se documenter à son tour afin de mieux apprécier le contenu du présent rapport.

---

### B. Proposition de l'encadrant

* *I) Le contexte et problème :* _quels sont les éléments à connaître pour comprendre le problème et ses motivations, quel est le problème adressé dans ce projet_
* *II) La solution :* _// Quelle est l’architecture (les principaux éléments) de votre solution_
* *III) La gestion du projet :* _// Quelle était l’organisation de votre équipe et les moyens utilisés pour atteindre l’objectif_
* *IV) Conclusion :* _// Qu’est ce qu’il faut retenir de votre projet_

—————
#### I) Le contexte et problème 

* *L’assistance aux personnes âgées* 
    * La situation nationale et internationale 
    * quels sont les problèmes liés au vieillissement de la population 
    * Quelles sont les solutions possibles 
    * La robotique comme solution 

* *la robotique* 
    * les robots humanoide 
    * le robot NAO
        * structure 
        * les caractéristiques 
        * la programmation: le langage 

* *les objets connectés (Internet des objet)*
    * Définition (le quoi ?)
    * Les motivations (le pourquoi ? )
    * Quelques problèmes liés à la mise en ouvre des objets connectés 

* *le problème (le sujet)* : assistance ax personnes âgées basés sur l’interaction avec le robot NAO et des objets connectés actionneurs. 

#### II) La solution 
1. *Définition des scénarios*: description et UML
2. Architecture global du système 
3. Réalisation scénario d’interaction 

    * scénario 1: nom du scénario
        * présenter les principaux éléments de la solution pour ce scénario
    * scénario 2 : …
        * même chose que scénario 1 
        ….

4. objets connectés 


#### III) La gestion du projet 

1. Planning (diagramme de gantt)
2. Organisation (agilité)
3. Difficultés

#### IV) Conclusion 

*Remarques :*
Ratio du nombre de pages
+ 50% à 60% : solution
+ 20%-25% : contexte/problème 
+ 10%-20% : gestion du projet 
+ Rest : Intro + conclusion + Refs

## 3. Changelog
* _19/04/17 :_ Mise en place du .tex et .bib. Rédaction du README.md avec explication des différentes parties du plan.
* _01/05/17 :_ Généralisation de la rubrique de second niveau "Plan", mise à jour des noms des contributeurs, ajout du plan proposé par l'encadrant.

## 4. Contributeurs

* Elodie TRIBOUT
* Cédric PLUTA		    [nayboko.fr](http://www.nayboko.fr)
