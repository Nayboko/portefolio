# Webographie

## Culture Générale
### Internet of Things
* https://aruco.com/2015/02/internet-des-objets-economie-accenture/
* Rapport de l'entreprise AKAMAI sur les vulnérabilités des appareils connectés <https://www.akamai.com/us/en/multimedia/documents/state-of-the-internet/sshowdown-exploitation-of-iot-devices-for-launching-mass-scale-attack-campaigns.pdf
* Rapport de l'ARCEP sur l'IoT <http://www.arcep.fr/uploads/tx_gspublication/livre_blanc_IoT-01-cartographie-071116.pdf>
* Article de sensibilisation à la sécurité réseau de lié aux objets connectés <http://www.journaldunet.com/solutions/expert/63362/comment-l-iot-bouscule-notre-securite.shtml>
* https://developer.microsoft.com/fr-fr/windows/iot
* https://developer.ubuntu.com/core

### Démographie
* http://www.senioractu.com/Causes-et-consequences-du-vieillissement-de-la-population-europeenne_a4298.html
* http://www.lefigaro.fr/conjoncture/2014/08/07/20002-20140807ARTFIG00011-le-vieillissement-ralentira-la-croissance-mondiale.php
* https://fr.wikipedia.org/wiki/Vieillissement_d%C3%A9mographique
* https://www.ncbi.nlm.nih.gov/pubmed/?term=Global%2C+regional%2C+and+national+age%E2%80%93sex+specific+all-cause+and+cause-specific+mortality+for+240+causes+of+death%2C+1990%E2%80%932013%3A+a+systematic+analysis+for+the+Global+Burden+of+Disease+Study+2013
* https://fr.wikipedia.org/wiki/Esp%C3%A9rance_de_vie_humaine#cite_note-6
* http://donnees.banquemondiale.org/indicateur/SP.DYN.TFRT.IN?end=2015&start=1960&view=chart
* https://societesnordiques.wordpress.com/2008/02/18/suede-la-prise-en-charge-de-la-dependance-des-personnes-agees/
* http://www.robert-schuman.eu/fr/questions-d-europe/0196-la-prise-en-charge-de-la-dependance-dans-l-union-europeenne
* http://www.cor-retraites.fr/IMG/pdf/doc-1447.pdf

### Automates et Robotique
* https://fr.wikipedia.org/wiki/Robot
* http://www.la-croix.com/Economie/France/Des-robots-pour-maintien-domicile-personnes-agees-2016-03-25-1200749238
* http://inno3med.fr/portfolio_item/paro/
* https://kompai.com/
* https://archive.org/details/CapekRUR
* https://www.ttu.ee/public/m/mart-murdvee/Techno-Psy/Isaac_Asimov_-_I_Robot.pdf
* https://fr.wikipedia.org/wiki/Jacques_Vaucanson
* https://fr.wikipedia.org/wiki/Al-Jazari
* http://www.lemonde.fr/pixels/article/2016/03/17/hiroshi-ishiguro-l-homme-qui-cree-des-robots-a-son-image_4884270_4408996.html
* https://fr.wikisource.org/wiki/L’Ève_future
* https://fr.scribd.com/doc/200133822/The-Book-of-Knowledge-of-Ingenious-Mechanical-Devices

### Raspberry Pi
* https://www.networkshare.fr/2016/07/21-idees-projets-raspberry-pi-2016/
* https://fr.wikipedia.org/wiki/Raspberry_Pi
* https://www.raspberrypi.org/products/model-b-plus/
* https://www.raspberrypi.org/products/raspberry-pi-2-model-b/

### Arduino - Genuino
* https://www.arduino.cc/en/main/arduinoBoardUno
* https://store.arduino.cc/arduino-uno-rev3
* https://zestedesavoir.com/tutoriels/686/arduino-premiers-pas-en-informatique-embarquee/742_decouverte-de-larduino/3414_presentation-darduino/
* https://www.makershed.com/products/make-getting-started-with-arduino-3rd-edition
* http://www.atmel.com/images/Atmel-8271-8-bit-AVR-Microcontroller-ATmega48A-48PA-88A-88PA-168A-168PA-328-328P_datasheet_Complete.pdf
* 

## Python
### Documentation Python 2.7
* Documentation officielle Python 2.x <https://docs.python.org/2/>
* Utilisation du module `socket` <https://www.ibm.com/developerworks/linux/tutorials/l-pysocks/>
* <http://www.journaldunet.com/developpeur/pratique/developpement/12309/comment-verifier-qu-un-fichier-existe-en-python.html>
* Gestion des erreurs <https://www.afpy.org/doc/python/2.7/tutorial/errors.html>
* Gestion des E/S <https://www.afpy.org/doc/python/2.7/tutorial/inputoutput.html>
* Information sur les dictionnaires <http://www.science-emergence.com/Articles/Cl%C3%A9-dun-dictionnaire-avec-plusieurs-valeurs-associ%C3%A9es-sous-python/>
* isinstance et basestring <http://stackoverflow.com/questions/4843173/how-to-check-if-type-of-a-variable-is-string>
* Gestion des exceptions <http://sametmax.com/gestion-des-erreurs-en-python/>
* Lecture/Ecriture dans un fichier <http://www.pythonforbeginners.com/files/reading-and-writing-files-in-python>

### Serveur TCP/IP
* Création de Client/Serveur TCP<https://pymotw.com/2/socket/tcp.html>
* Documentation du module `socket` <https://docs.python.org/2/library/socket.html?highlight=socket#module-socket>
* Documentation du module `socketserver` <https://docs.python.org/2/library/socketserver.html>
* <https://pythontips.com/2013/08/06/python-socket-network-programming/>
* Client serveur avec prise en charge paquets JSON <http://lesoluzioni.blogspot.fr/2015/12/python-json-socket-serverclient.html>

### Socket avec Python 2.x
* Récupérer son adresse IPv4 sur son réseau local <http://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib>
* Libérer un port <http://stackoverflow.com/questions/4465959/python-errno-98-address-already-in-use>
* Exemple de serveurs <http://www.binarytides.com/python-socket-server-code-example/>

### Module Subprocess
* <https://docs.python.org/2/library/subprocess.html>
* Gestion timeout <https://dzone.com/articles/python-101-how-to-timeout-a-subprocess>
* <https://docs.python.org/2/library/subprocess.html#subprocess.Popen>
* Communication entre processus <https://www.afpy.org/forums/forum_python/forum_general/878908851653>

### Module Sys
* Récupération de la liste des arguments <https://openclassrooms.com/forum/sujet/python-recuperer-des-arguments-72479>
* Ecrire dans le canal stdout sans retour chariot <http://stackoverflow.com/questions/511204/how-to-print-a-string-without-including-n-in-python>

### Module Serial
* Installation du module Serial <http://stackoverflow.com/questions/33267070/no-module-named-serial>

## Réseau
* Différence Synchrone et Asynchrone <http://orsypfrance.typepad.com/blog/2011/03/t%C3%A2ches-synchrones-asynchrones-quel-impact-sur-le-s%C3%A9quen%C3%A7age-et-le-monitoring-.html>
* Fonctions pour vérifier qu'un périphérique est connecté <https://www.developpez.net/forums/d1403774/autres-langages/python-zope/reseau-web/ping-sous-python/>
* Copie de fichiers avec le protocole ssh <http://www.hypexr.org/linux_scp_help.php>

## Général
* Mettre en couleur dans un terminal avec la norme ANSI <http://jafrog.com/2013/11/23/colors-in-terminal.html>
* Ajouter un script au démarrage d'une machine Unix<https://blog.nicolargo.com/2009/03/creation-dun-script-de-demarrage-sous-linux.html>
* Run-time dependencies for init script <https://wiki.debian.org/LSBInitScripts>
* EPYDOC: Génération de documentation API pour Python <http://deusyss.developpez.com/tutoriels/Python/Epydoc/#LI> 
* Utiliser EPYDOC <http://epydoc.sourceforge.net/manual-usage.html>
* Transfert de fichier en SSH avec `scp` <https://blog.v-jeremy.net/scp-transfert-de-fichier-via-ssh/>
* https://www.lifewire.com/usb-type-b-connector-2626033
