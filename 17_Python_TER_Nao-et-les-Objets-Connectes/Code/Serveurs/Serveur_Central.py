#!/usr/bin/python
# coding=utf-8

import Serveur
import sys
import subprocess
import time

class Serveur_Central(Serveur.Serveur):
	
	# Retourne une liste des mots du message reçu
	def make_paquet(self, data):
		paquet = []
		if data is not None:
			for mot in data.split(' '):
				paquet.append(mot)
			return paquet
		else:
			return None

	def remake_bd(self, obj, msg):
		content = ""
		message = msg.split(" ")
		f_nom = obj+".txt"
		path = "./BD/"+f_nom
		print >>sys.stderr, self.out_handle + "Ouverture du fichier de la Base de Données "+ path
		file = open(path, "r")

		f = file.readlines()

		file.close()
		for line in f:
			mot = line.split(",")
			if mot[0] == message[0]:
				mot[3] = message[1]
				content += mot[0]+","+mot[1]+","+mot[2]+","+mot[3]+"\n"
			else:
				content += line

		file = open(path, "w")
		print >>sys.stderr, self.out_handle + "Modification de la Base de Données "+ path
		file.write(content)
		print >>sys.stderr, self.out_handle + "Fermeture de la Base de Données "+ path
		file.close()


	def handle(self, msg):

		
		m = msg
		# On génère un tableau pour séparer les mots reçus
		paquet = self.make_paquet(msg)
		if paquet != None:
			print >>sys.stderr, self.out_handle +"Interprétation du paquet ", paquet

			# Le format des messages est <Qui?> <Quoi?> <Ordre A/E/I>
			if len(paquet) != 3:
				print >>sys.stderr, self.out_handle+ "Le paquet", paquet, "n'est pas interprétable !"
				_historic=""
				for i in paquet:
					_historic += i+"_"
				self.historic("Erreur Paquet_"+_historic+" Incorrect")
				self.C_sock.sendall("Oups, je ne peux pas exécuter cet ordre.")
				pass

			elif len(paquet) == 3:
				print >>sys.stderr, self.out_handle + "Le paquet est un ordre vers l'objet connecté "+ str(paquet[0])
				print >>sys.stderr, self.out_handle + "Execution du script d'interpretation de la Base de Données"

				# On lance l'interpreteur de BD pour obtenir
				# les informations de l'objet concerné
				incoming = self.make_subprocess("python", "./InterpreteurBD.py", paquet)
				time.sleep(0.1)

				print >>sys.stderr, self.out_handle + "Fin de l'interprétation. Résultat de la lecture de la Base de Données: " + incoming[0]

				# Génération d'un paquet à envoyer à l'objet
				# concerné par le message initial
				order = self.make_paquet(incoming[0])

				## SI AUCUNE COMMANDE ASSOCIEE A L'ORDRE DE L'UTILISATEUR
				if order[2] == "None":
					print >>sys.stderr, self.out_handle +" Erreur, "+paquet[1]+" ne semble pas exister dans la BD ou l'ordre associé n'est pas possible"
					self.historic("Erreur " + paquet[0]+"_"+paquet[1]+ " No-Exist")
					self.C_sock.sendall(paquet[1]+ " n'existe pas dans ma Base de Données")
				else:					
					## SINON EXECUTION DE LA COMMANDE

					# Si "arduino chambre I" (par exemple), on retourne l'état de chambre en fonction de ce qui a été lu dans la base de données

					# REQUETE D'INFORMATION
					if paquet[2] == 'I':
						etat = self.out_handle
						if order[0] == "I/O":
							etat += "Erreur I/O, la lecture de la BD a échoué. L'objet " + paquet[0]+ " ne semble pas exister. Veuillez ressaisir votre message."
							self.historic("Erreur BD I/O")
						else:
							etat += "Selon "+paquet[0]+"  d'adresse "+order[0]+"/"+order[1]+ ", "+paquet[1]+" est actuellement dans l'etat "
							repNao = paquet[1] + " est "
							if order[2] == "allume" or order[2] == "ouvert":
								etat += self.color_Bgreen + "ON" + self.color_end
								repNao += order[2]

							elif order[2] == "eteint" or order[2] == "ferme":
								etat += self.color_Bred + "OFF" + self.color_end
								repNao += order[2]
							else:
								etat += self.color_Bpink + "Indeterminé" + self.color_end
								etat += ". Cette pièce ne semble pas être référencée dans la Base de Données."
								self.historic("Erreur "+paquet[1]+" Non-existant")
						print >>sys.stderr, etat
						print >>sys.stderr, self.out_handle + "Réponse à Nao : " + repNao
						# RETOURNE repNao A NAO L'ETAT DE LA REQUETE
						self.historic(repNao)
						self.C_sock.sendall(repNao)

					# Si order[0] qui correspond à l'adresse IP d'un objet est NULL
					# Cela signifie que ce n'est pas un objet connecté par internet
					# Et donc que c'est une carte Arduino directement connecté
					# au serveur central.

					# PARTIE ARDUINO #

					if order[0] == "NULL" and paquet[2] != "I":

						# On lance l'interpreteur d'ordre de la Arduino
						print >>sys.stderr, self.out_handle + "Execution du script d'interpretation de la carte Arduino dans un sous-processus"
						incoming = self.make_subprocess("python", "./Domotarduino.py", order)
						time.sleep(0.1)
						if incoming[0] == "ok\r\n":
							print >>sys.stderr, self.out_handle + "L'ordre a bien été executé."
							# MESSAGE DE RETOUR A NAO
							repNao = paquet[1] + " a bien été "
							self.tmp = paquet[1] + " "
							if paquet[2] == 'A':
								repNao += "allumée"
								self.tmp += "allume "
							elif paquet[2] == 'E':
								repNao += "éteint"
								self.tmp += "eteint "
							
							self.tmp += "ok"
							self.remake_bd(paquet[0], self.tmp)
							self.historic(self.tmp)
							self.C_sock.sendall(repNao)


						else:
							print >>sys.stderr, self.out_handle + "L'ordre semble avoir été bien executé. Cependant, le périphérique " + order[1]+ \
							" n'a pas répondu. " + incoming[0]
							self.historic("Erreur "+ order[0] +" Communication")
							self.C_sock.sendall("Il y a eu une erreur de communication avec la carte Arduino")	

					# PARTIE RASPBERRY PI / Micro-controleur LAN #

					elif order[0] != "NULL" and paquet[2] != "I": #Si on a une adresse IP
						print >>sys.stderr, self.out_handle + "Lancement de la vérification de connexion du périphérique " + order[0] + ". " \
						+ "Veuillez patienter quelques instants."

						# On teste la connexion au périphérique		
						isok = self.isConnect(order[0])
						if isok == "offline":
							print >>sys.stderr, self.out_handle + "Le périphérique "+order[0]+" est "+ self.color_Bred + "DISCONNECTED" + self.color_end
							self.historic("Info "+order[0]+" DISCONNECTED")
							# MESSAGE ERREUR POUR NAO
							self.C_sock.sendall("Erreur : Le périphérique est actuellement deconnecté")

						elif isok == "online":
							print >>sys.stderr, self.out_handle + "Le périphérique "+order[0]+" est "+ self.color_Bgreen + "CONNECTED" + self.color_end
							self.historic("Info "+order[0]+" CONNECTED")
							print >>sys.stderr, self.out_handle + "Redirection du message vers le périphérique "+order[0]

							print >>sys.stderr, self.out_handle + "Création d'un paquet"
							# [IP, Quoi?, A/E ou autre ordre]
							self.tmp = order[0] + " " + paquet[1] +" " + order[2]
							print >>sys.stderr, self.out_handle + "Envoie du paquet"
													
							fwd_data = self.forwarding(order[0], order[1], self.tmp)
							print >>sys.stderr, self.out_handle + "Réponse :", fwd_data 
							
							self.tmp = fwd_data.split(' ')

							if self.tmp[0] == "Error":
								repNao = fwd_data
							else:
								repNao = self.tmp[0] + " a bien été " + self.tmp[1]	

							# Changement dans la BD
							self.remake_bd(paquet[0], fwd_data)

							# Retourne fwd_data à Nao
							self.historic(fwd_data)
							self.C_sock.sendall(repNao)

						else:
							print >>sys.stderr, self.out_handle + "Le périphérique "+order[0]+" est "+ self.color_Bpink + "INDETERMINED" + self.color_end
							self.historic("Erreur "+order[0]+" INDETERMINED")
							self.C_sock.sendall("Oups, je n'arrive pas à connaître l'état de "+ order[0])							
						# On retourne l'ordre au serveur interne du périphérique



		else:
			print >>sys.stderr, self.out_handle,"Erreur, le message reçu était peut être corrompu. Veuillez relancer l'ordre."
			self.historic("Erreur Paquet Corrompu?")
			self.C_sock.sendall("Erreur, le message que j'ai fait parvenir au serveur a été corrompu. Veuillez renouveler votre demande.")


		
if __name__ == "__main__":
	serveur_central = Serveur_Central()