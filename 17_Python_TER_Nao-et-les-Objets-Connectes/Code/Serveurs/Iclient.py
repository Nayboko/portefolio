#!/usr/bin/python
# coding=utf-8

import socket
import sys
import time

def getip():
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        myip = s.getsockname()[0]
        return myip

print >>sys.stderr, 'Démarrage du client'

# Create a TCP/IP socket
Send = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
host=sys.argv[1]
port=int(sys.argv[2])
server_address = (host, port)
print >>sys.stderr, '# From %s #' % getip() 
print >>sys.stderr, '# Connecting to %s port %s #' % server_address
Send.connect(server_address)
data = None

try:
    # Send data
    while True:
        sys.stdout.write(">> ") 
        message = raw_input()
        if message == "close()":
            print >>sys.stderr, 'End of the programme because of ', message            
            break
        else:
            print >>sys.stderr, 'Sending ', message
            Send.sendall(message)
            time.sleep(0.1)
            data = Send.recv(512)
            print >>sys.stderr, "Answer : ", data

finally:
    print >>sys.stderr, 'Closing socket'
    Send.close()