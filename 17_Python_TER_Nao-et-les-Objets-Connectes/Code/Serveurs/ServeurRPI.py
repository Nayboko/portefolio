#!/usr/bin/python
# coding=utf-8

import Serveur_Central
import sys
import subprocess
import time

class ServeurRPI(Serveur_Central.Serveur_Central):

	def handle(self, msg):

		CSI = "\x1B["
		color_cyan = CSI+"36;1m"
		color_red = CSI+"31;1m"
		color_green= CSI+"32;1m"
		color_blue = CSI+"34;1m"
		color_pink = CSI+"35;1m"
		color_end = CSI+"0m"

		repNao = ""

		out_handle = color_cyan + "Traitement : "+ color_end

		_recv = msg
		print >>sys.stderr, out_handle + "Réception d'un paquet d'ordre : " + _recv
		print >>sys.stderr, out_handle + "Transformation en un paquet interprétable"
		paquet = self.make_paquet(_recv)

		# TO DO: Vérifier que le paquet est bien destiné au serveur
		# a l'aide du premier élément du paquet _recv

		if _recv is not None and len(paquet) == 3:
			print >>sys.stderr, out_handle + "Interprétation du paquet réceptionné pour l'ordre suivant: " + paquet[2]
			_answer = self.make_subprocess("python", "./InterpreteurRPI.py", paquet[2])

			time.sleep(0.1)
			print >>sys.stderr, out_handle + "Fin de l'interprétation du paquet réceptionné : " + str(_answer[0])

			# Vérifie qu'il y a bien ok à la fin du message _answer

			l = _answer[0].split(' ')

			if l[len(l) - 1] == "ok":
				print >>sys.stderr, out_handle + "Interprétation du paquet réussie avec succès"
				print >>sys.stderr, out_handle + "Réponse RPI : " + str(_answer[0])

				repNao = str(_answer[0])
				
			else:
				print >>sys.stderr, out_handle + "Réponse : " + str(_answer[0])
				repNao = str(_answer[0])
		else:
			print >>sys.stderr, out_handle + "Le paquet " + str(paquet) + " n'est pas interprétable."
			repNao = "L'execution de cet ordre m'est impossible."

		self.C_sock.sendall(repNao)

if __name__ == "__main__":
	sRPI = ServeurRPI()
