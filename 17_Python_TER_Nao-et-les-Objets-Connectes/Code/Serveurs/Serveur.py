#!/usr/bin/python
# coding=utf-8

import socket
import sys
import subprocess
import time

from datetime import datetime

class Serveur(object):
	# CONSTRUCTEUR : port et listen_max par défaut
	def __init__(self, port=8800, listen_max=5):
		self.host = self.getip()
		self.port = port
		self.msg = ""
		self.msg_cpt = 0
		self.serveur_cpt = 0
		self.loop = True
		self.tmp = None ### Variable temporaire pour tout et n'importe quoi

		## Affichage de convivialité
		self.CSI = "\x1B["
		self.color_cyan = self.CSI+"36;1m"
		self.color_yellow = self.CSI+"33;1m"
		self.color_red = self.CSI+"31;47m"
		self.color_green= self.CSI+"32;47m"
		self.color_blue = self.CSI+"34;1m"
		self.color_pink = self.CSI+"35;1m"
		self.color_Bred = self.CSI+"31;1m"
		self.color_Bgreen= self.CSI+"32;1m"
		self.color_Bblue = self.CSI+"34;1m"
		self.color_Bpink = self.CSI+"35;1m"
		self.color_end = self.CSI+"0m"

		## Variables pour la socket Serveur """
		self.S_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		## Rend le port réutilisable après fermeture
		self.S_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
		self.liste_client = {self.port : self.host}
		self.S_adr = (self.host, port)
		self.out_historic = self.color_yellow + "Historique : " + self.color_end
		self.out_serveur = self.color_blue + "Serveur[%s:%s]: " % self.S_adr + self.color_end
		self.out_handle = self.color_cyan + "Traitement : "+ self.color_end

		## Variables pour la socket Serveur
		self.C_sock = None
		self.C_adr = None

		self.S_sock.bind(self.S_adr) ### Nommage
		self.S_sock.listen(listen_max) ### Mise en écoute
		print >>sys.stderr, "++++ Connexion ++++"
		self.serveur_on() ### Activation du serveur

	# DESTRUCTEUR
	def __del__(self):
		print "---- Déconnexion ----"
		self.S_sock.close()

	# Retourne les info inhérentes au serveur local
	def info(self):
		"""
		@param self: Serveur
		@type self: Class Serveur
		@return: Retourne les informations concernant le serveur
		@rtype: String
		"""
		now = self.serveur_time()
		i = 1
		s = "\nInformation sur le serveur : \n"
		s += ">> "+"Heure locale : "+str(now[0])+":"+str(now[1])+":"+str(now[2])+" \n"
		s += ">> "+"IP = "+str(self.host)+" \n"
		s += ">> "+"Port = "+str(self.port)+"\n"
		s += ">> "+"Paquets reçus = "+str(self.serveur_cpt)+"\n"
		for cle, valeur in self.liste_client.items():
			s += "Client n°"+str(i)+" port: "+str(cle)+" ~~ IP: "+str(valeur)+"\n"
			i += 1
		return s

	def historic(self, msg):
		
		if msg != None:
			now = []
			now = datetime.now()

			l= [now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond]
			
			print >>sys.stderr, self.out_historic + "Traitement pour l'enregistrement de l'évènement"

			#pour ouvrir ou créer un fichier dans un dossier de l'arborescence
			f_nom = "Historique-" + str(l[0]) + "-" + str(l[1]) + "-" + str(l[2])+ ".txt"
			path = "./historique_ordres/" + f_nom

			stg = msg.split(' ')
			if stg[0] == "Error":
				s = str(l[0])
				for i in range(1,len(l)):
					s += "-" + str(l[i])
				s += " : [ "+msg+" ] \n"
			else:
				ordre = "[" + stg[0] + " " + stg[1] +"] -> "
				s = str(l[0])

				for i in range(1,len(l)):
					s += "-" + str(l[i])
				s += " : " + ordre + " " + stg[2] + "\n"

			print >>sys.stderr, self.out_historic + "Ouverture fichier " + f_nom
			try:
				f = open(path,"a+")
				f.write(s)
				f.close()
			except IOError as e:
				print >>sys.stderr, self.out_historic + str(e)
			finally:
				print >>sys.stderr, self.out_historic + "Fermeture fichier"

		else :
			print >>sys.stderr, self.out_historic + "Erreur : msg vide"

	# Retourne l'heure locale du serveur
	def serveur_time(self):
		now = datetime.now()
		l = [now.hour, now.minute, now.second]
		return l

	# Activation du serveur
	def serveur_on(self):
		print >>sys.stderr, "Serveur : Démarrage en %s au port %s" % self.S_adr
		while self.loop:
			self.serve()

	# Désactivation du serveur
	def serveur_off(self):
		print >>sys.stderr, "Serveur[%s:%s]: Arrêt des services en cours ..." % self.S_adr
		self.loop = False

	# Récupération de l'IP de la machine courante
	def getip(self):
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.connect(("8.8.8.8", 80))
		myip = s.getsockname()[0]
		s.close()
		return myip

	# Envoie un ping à une machine distante d'adresse ip, et retourne la réponse sous
	# forme d'une liste de lignes de réponses
	def ping_ip(self, ip, encodage="utf8"):
		cmd = ["ping", "-c", "2", ip]
		try:
			out, _ = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()
		except (ValueError, OSError) as e:
			return ("Erreur: %s" % (e.args[0],)).decode(encodage)
		rep = out.decode(encodage)
		return rep.splitlines()

	# Retourne si ip est connecté ou non
	def isConnect(self, ip):
		lignes = self.ping_ip(ip)
		k = 0

		if lignes[0] == "ping: unknown host" + ip :
			return ip + u" le périphérique n'existe pas sur le réseau"
		else:
			for i, ligne in enumerate(lignes[1:]):
				if len(ligne) == 0:
					k = i + 1
					break
			if len(lignes) - k > 3 :
				#print "online"
				return "online"
			else:
				#print "offline"
				return "offline"

	# Retourne un paquet vers un client/serveur externe
	def forwarding(self, ip, port, msg):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		fwd_ip = ip
		fwd_port = int(port)
		fwd_adr = (fwd_ip,fwd_port)
		
		try:
			s.connect(fwd_adr)
			fwd_msg = msg
			s.sendall(msg)
			time.sleep(0.1)
			data = s.recv(512)
			return data
		except Exception as e:
			return "Erreur : "+ str(e)


	# Création d'un processus fils et récupération de la sortie standard
	def make_subprocess(self, cmd, target, args):
		incoming = ""
		if len(args) == 3:
			p = subprocess.Popen([cmd, target, args[0], args[1], args[2]], stdout=subprocess.PIPE)
			time.sleep(0.1)
			incoming = p.communicate()
		elif len(args) == 2:
			p = subprocess.Popen([cmd, target, args[0], args[1]], stdout=subprocess.PIPE)
			time.sleep(0.1)
			incoming = p.communicate()
		elif isinstance(args, basestring): # Si args est juste un string
			p = subprocess.Popen([cmd, target, args], stdout=subprocess.PIPE)
			time.sleep(0.1)
			incoming = p.communicate()
		return incoming

	# Traitement de messages généré par des clients externes
	def handle(self, msg):
		raise NotImplementedError('handle() doit être surchargé dans une sous-classe')

	# Services principaux
	def serve(self, timeout=10):
		# Encodage couleur ANSI
		# Pour une meilleur visibilité sur la console

		

		print >>sys.stderr, self.out_serveur + "En attente de connexion ..."
		self.C_sock, self.C_adr = self.S_sock.accept()
		self.liste_client[self.C_adr[1]] = {self.C_adr[0]}

		out_client = self.color_pink + "Client[%s:%s]: " % self.C_adr + self.color_end

		try:
			print >>sys.stderr,  out_client + self.color_green + " Connexion " + self.color_end
			while True:
				print >>sys.stderr, self.out_serveur + "En attente de message du client[%s:%s] " % self.C_adr
				self.msg = self.C_sock.recv(512)

				# Si il existe un message, on le prend en charge
				if self.msg:
					try:
						self.msg_cpt+=1
						self.serveur_cpt+=1
						print >>sys.stderr, self.out_serveur + "Réception du message " + self.msg

						# Accusé de réception
						# NOTE : On désactive ici l'accusé de réception afin de pouvoir retourner
						# les réponses aux ordres du client
						
						#rep = "L'ordre "+ self.msg +" a bien été réceptionné. ID-REQUETE = " + str(self.serveur_cpt)
						#self.C_sock.sendall(rep)
						# Commande d'arrêt du serveur
						if self.msg == "stop":
							self.serveur_off()
							self.C_sock.sendall("Le serveur central est en cours d'arrêt")
							break
						elif self.msg == "info":
							print self.info()
							self.C_sock.sendall('Le serveur central est allumé')
							continue
					except:
						print >>sys.stderr, self.out_serveur + "Erreur dans le protocole de réception du message  " + self.msg
						continue

					# Traitement du message
					self.handle(self.msg)

				else:
					print >>sys.stderr, self.out_serveur + "Aucune donnée supplémentaire de ", self.C_adr
					break

		finally:
			print >>sys.stderr, self.out_serveur + "Nombre de messages reçus par %s:%s :" % self.C_adr, self.msg_cpt
			print >>sys.stderr, out_client + self.color_red + " Déconnexion " + self.color_end
			self.msg_cpt = 0
			self.C_sock.close()

if __name__ == "__main__":
	serveur = Serveur()
