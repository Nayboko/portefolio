/*
 * 3 Leds sont reliées à l'Arduino
 * Les leds s'allument en fonction du message reçu
 */
 
// Déclaration des variables globales
#define LED1  2
#define LED2  3
#define LED3  4
#define N  3 // Nombre de LEDS

int state[N] = {0}; // 0 si Led éteinte, 1 sinon


int msg = 0;

void setup() {
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if(Serial.available()){
    msg = Serial.read() - '0'; // Soustraction de 0 qui vaut 48 en ASCII

    // Si msg reçu correspond [1,2,3], on change l'état de la LED sur 1
    for(int i = 1; i <= N; i++){
      if(msg == i){
        state[i -1] = 1;
        delay(50);
      }
    }

    // Si msg € [4,5,6], on change l'état de la LED sur 0 
    for(int j = 4; j <= 2*N; j++){
      if(msg == j){
        state[j - 4] = 0;
        delay(50);
      }
    }

    // On allume ou éteint les LEDS en fonction de leur état
    if(state[0] == 1){
      digitalWrite(LED1, HIGH);
    }else{
      digitalWrite(LED1, LOW);
    }
    if(state[1] == 1){
      digitalWrite(LED2, HIGH);
    }else{
      digitalWrite(LED2, LOW);
    }
    if(state[2] == 1){
      digitalWrite(LED3, HIGH);
    }else{
      digitalWrite(LED3, LOW);
    }
    
  }
}
