// Blinking the embended LED
/*  Note: Il est possible d'utiliser la technique du PWM (pulse-width modulation)
    Pour faire intervenir le phénomène de persistance rétinienne (POV Persistance of Vision)
    Il faut régler les delay à 10ms */

const int LED = 13; // LED connectée au
                    // pin digital 13

void setup() {
  pinMode(LED, OUTPUT); // Mets le pin digital sur Output
}

void loop() {
  digitalWrite(LED, HIGH);  // Allume la LED
  delay(1000);              // Attend 1 seconde
  digitalWrite(LED, LOW);   // Eteint la LED
  delay(1000);              // Attend 1 seconde
}
