<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Scenario_coucher" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="s_coucher" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="diag_coucher" src="s_coucher/diag_coucher/diag_coucher.dlg" />
        <Dialog name="diag_fatigue" src="s_coucher/diag_fatigue/diag_fatigue.dlg" />
    </Dialogs>
    <Resources>
        <File name="volet_ferme" src="volet_ferme.png" />
        <File name="volet_ouvert" src="volet_ouvert.png" />
    </Resources>
    <Topics>
        <Topic name="diag_coucher_frf" src="s_coucher/diag_coucher/diag_coucher_frf.top" topicName="" language="C" />
        <Topic name="diag_fatigue_frf" src="s_coucher/diag_fatigue/diag_fatigue_frf.top" topicName="diag_fatigue" language="fr_FR" />
    </Topics>
    <IgnoredPaths />
</Package>
