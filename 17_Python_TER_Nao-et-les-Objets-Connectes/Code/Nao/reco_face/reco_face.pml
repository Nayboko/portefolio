<?xml version="1.0" encoding="UTF-8" ?>
<Package name="reco_face" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="Reco_face" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="learn_reco" src="Reco_face/learn_reco/learn_reco.dlg" />
    </Dialogs>
    <Resources />
    <Topics>
        <Topic name="learn_reco_frf" src="Reco_face/learn_reco/learn_reco_frf.top" topicName="learn_reco" language="fr_FR" />
    </Topics>
    <IgnoredPaths />
</Package>
