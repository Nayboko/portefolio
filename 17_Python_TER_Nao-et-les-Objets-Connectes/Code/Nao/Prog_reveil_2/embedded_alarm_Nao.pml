<?xml version="1.0" encoding="UTF-8" ?>
<Package name="embedded_alarm_Nao" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="AlarmNao" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="alarm_diag" src="alarm_diag/alarm_diag.dlg" />
    </Dialogs>
    <Resources>
        <File name="Reveil_en_douceur" src="Reveil_en_douceur.mp3" />
    </Resources>
    <Topics>
        <Topic name="alarm_diag_frf" src="alarm_diag/alarm_diag_frf.top" topicName="" language="C" />
    </Topics>
    <IgnoredPaths />
</Package>
