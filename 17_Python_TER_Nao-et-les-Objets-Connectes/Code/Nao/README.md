## Généralité

Le projet Choregraphe "ServeurNao" est la dernière version de travail de notre projet.

## Reste à intégrer:

* Turn_and_Walk
* Walk_Tool
* Reco_Face et Reco_Learn_Face
* Prog_reveil et les alarm_script

My_first_interactive permet de comprendre comment mettre en place des comportements dynamiques et solitaires avec Nao.

## Non-fonctionnels

* Les scripts d'alarme
* reco_face2
* Finaliser l'implémentation des alarmes dans prog_reveil et prog_reveil2