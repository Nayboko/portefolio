#!/usr/bin/env python
# -*- coding: utf-8 -*-

from alarm import Alarme

print(" Alarme Programmee ")

alarm_A = input("Année : ")
alarm_M = input("Mois : ")
alarm_J = input("Jour : ")
alarm_H = input("Heure : ")
alarm_Mn = input("Minute : ")
alarm_E = raw_input("Evenement : ")

print("Vous avez entré les valeurs suivantes : " + str(alarm_A))
print(alarm_A)
print(alarm_M)
print(alarm_J)
print(alarm_H)
print(alarm_Mn)
print(alarm_E)

alarm = Alarme(alarm_A, alarm_M, alarm_J, alarm_H, alarm_Mn, alarm_E)
alarm.start()

try:
	while True:
		text = str(raw_input())
		if text == "stop":
			alarm.alarm_kill()
			break
except:
	print("Erreur")
	alarm.alarm_kill()		