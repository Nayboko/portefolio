#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from datetime import datetime
from threading import Thread

class Clock(Thread):
	def __init__(self):
		# initialisation
		super(Clock, self).__init__()
		self.year = 0
		self.month = 0
		self.day = 0
		self.hour = 0
		self.minute = 0
		self.second = 0
		# intervalle de màj en seconde
		self.maj_intervalle = 1
		self.active = True

	def run(self):
		try:
			while self.active:
				# màj de Clock
				now = datetime.now()
				self.year = now.year
				self.month = now.month
				self.day = now.day
				self.hour = now.hour
				self.minute = now.minute
				self.second = now.second

				print("System time : "+str(clock.hour)+":"+str(clock.minute)+":"+str(clock.second))
				time.sleep(self.maj_intervalle)
			return
		except:
			return

	def current(self):
		data = []
		data.append(self.year)
		data.append(self.month)
		data.append(self.day)
		data.append(self.hour)
		data.append(self.minute)
		data.append(self.second)

		return data

	def quit(self):
		self.active = False

if __name__ == '__main__':
	clock = Clock()
	clock.start()

	try:
		while True:
			text = str(raw_input())
			if text == "stop":
				clock.quit()
				break
	except:
		print("Erreur")
		clock.quit()

	