# Nao et les Objets Connectés

## Résumé

C’est dans le cadre de l’exploitation et l’application de nos connaissances acquises lors de notre cursus en Licence Informatique que la réalisation de ce Travail d'Etudes et de Recherches s'incrit.

Le but était d'établir des scénarii d'assistance aux personnes dépendantes en utilisant le robot Nao ainsi qu'une système domotique.

Le projet est principalement implémenté en Python 2.7.x pour la partie robotique et en C-Arduino/Python pour la partie domotique.

## Implémentation

Pour le robot Nao, l'implémentation se fait à travers le logiciel Choregraphe dont les modules sont écrits en Python 2.7.x.

Plusieurs modules ont été ajoutés à Nao, notamment un module de reconnaissance faciale, de déplacement et de discussion. 

Pour le système domotique, il était constitué de cartes Arduino permettant la simulation d'objets connectés (fenêtres, portes, lumières, etc) et de serveurs dans des Raspberry Pi constitués d'un serveur central, de serveurs auxiliaires, d'un gestionnaire d'historique et d'interpréteurs d'ordres (Nao vers le système et inversement).

## Installation et Exécution

L'ensemble de la procédure se situe dans le fichier "Utilisation.md".

## Démonstration

La vidéo de notre projet se trouve dans le dossier "Soutenance" sous le nom "Demo_Nao.mp4".

## En savoir plus

Dans le dossier "Rapport" : 
* le fichier "scerario_nao_v1.pdf" décrit les scénarii d'assistance à des personnes dépendantes et/ou handicapées en utilisant Nao couplé à de l'IoT (Internet of Things) ;
* le fichier "iot_20160127.pdf" qui est un compte-rendu de recherches sur l'IoT et ses différentes applications ;
* le fichier "Rapport-v1.0/rapport_naoObj_1.0.pdf" est le rapport de stage comprennant une étude de cas, un état de l'art et la description de notre solution ;
* "Rapport-v1.0/README.md" est un bref résumé du rapport.