#!/usr/bin/env python3

########################################
# Program: Kamon                       #
# Version: 1.0.0                       #
# Last Update: 2016/04/05              #
########################################

import random
from tkinter import *
import tkinter as tk
from math import *

#======================================#
#             Color Codes              #
#======================================#
argent= "#CECECE"
beige=  "#F3E2A9"
caramel= "#985717"
noirCnoir= "#303030"

#======================================#
#           Global variables           #
#======================================#

# Version number
version = "Kamon v.1.0.0"
# Window
KWindow=tk.Tk()
KCanvas=tk.Canvas()
Height = 450    # (Y-axis)
Width = Height  # (X-axis)
fontTitle = "Helvetica 20 bold underline"
fontPlayer= "Helvetica 15 bold"
KJ1 = []        #idcircle,idText
KJ2 = []        #idcircle,idText
lastKamon = []  #idcircle,idText
strg=Label(KWindow)
helpBool=False

# Player variables
player = 'black'    # First player is black
cptB = 0            # Round counter
cptW = 0
tokenB = []         # List of the pawns played by each player
tokenW = []

# Game Variables
turn = "Text"
ident = 0
idText = 0

# Kamon tokens
centerX = Width*(131./400.)
centerY = Height*(81./400.)
radius = ((Height*23)/400)
centerList = []
listKamon=[]#color, number, key, x, y, idcercle,idtext
keyKamon=[]
base = 4
nbCircles = (base * base) + (base * (base - 1)) + (base - 1)**2
Ktmp=[] # Recovery current token for LastKamon
KPlayable=[2,3,5,9,10,15,23,28,29,33,35,36]

# Graph variables
nodeList = []
graph = []
graphB = grapheW = []
sideList = [[4,3,2,1],[1,5,10,16],[16,23,29,34],[34,35,36,37],[37,33,28,22],[22,15,9,4]]

# Token colors
    # Green Blue Red Orange Yellow Purple
Color=['#16B84E','#0ABAB5','#FD4626','#FA961E','#FCDC12','#C9A0DC']

#======================================#
#           Defining Functions         #
#======================================#

def initWindow():
    """ 
    name:   initWindow
    brief:  Initialise la fenêtre de jeu
    param:  None 
    """
    global KWindow, KCanvas, KJ1, KJ2, lastKamon, strg, helpBool
    
    # ~~~~ Canvas initialization ~~~~ #

    # Window generation
    KWindow.title(version)
    
    # Navigation menu generation 
    menuTool=Menu(KWindow)
    menu1=Menu(menuTool, tearoff=0)
    menu1.add_command(label="Nouvelle partie", command=newGame)
    menu1.add_separator()
    menu1.add_command(label="Quitter", command=callBack)
    menuTool.add_cascade(label="Fichier", menu=menu1)
    menu2 = Menu(menuTool, tearoff=0)
    menu2.add_command(label="Activer l'aide du jeu", command=helpJ)
    menu2.add_command(label="Désactiver l'aide du jeu", command=removeHelpJ)
    menuTool.add_cascade(label="Affichage", menu=menu2)
    menu3 = Menu(menuTool, tearoff = 0)
    menu3.add_command(label="2 joueurs")
    menu3.add_command(label="Contre une IA")
    menu3.add_command(label="2 IA")
    menuTool.add_cascade(label="Mode", menu=menu3)
    menu4 = Menu(menuTool, tearoff=0)
    menu4.add_command(label="À propos",command=us)
    menu4.add_command(label="Règles du jeu",command=rules)
    menuTool.add_cascade(label="Aide",menu=menu4)
    KWindow.config(menu=menuTool)
    
    # Canvas generation
    KCanvas = tk.Canvas(KWindow, height = Height, width = Width, background = argent)
    KCanvas.pack()
    
    # ~~~~ Board initialization ~~~~ #
    initCenter()
    
def helpJ():
    """ 
    name:   helpJ
    brief:  Active l'aide de jeu
    param:  None
    """
    global helpBool
    
    helpBool=True
    helpGame('blue',4)
    
def removeHelpJ():
    """ 
    name:   removeHelpJ
    brief:  Désactive l'aide de jeu
    param:  None
    """
    global helpBool
    
    helpBool=False
    helpGame(noirCnoir,2)
    
def play():
    """ 
    name:   play
    brief:  Génère les élements de jeu
    param:  None 
    """
    global KCanvas, strg
    
    # ~~~~ Grid and Hexagon ~~~~ #
    KBoard = KCanvas.create_polygon( ((Width*9)/32),(Height/8), ((Width*23)/32),(Height/8), ((Width*15)/16),(Height/2),((Width*23)/32),((Height*7)/8), ((Width*9)/32),((Height*7)/8), (Width/16),(Height/2), ((Width*9)/32),(Height/8), fill = caramel,  outline = "black", width=5)

    # Title 
    title=KCanvas.create_text((Width/2), (Height/16), text="Le Kamon", font= fontTitle, fill ='black')

    # Random placement of Kamons:
    setKamon()
    setBoard()
    
    # ~~~~  Player turn management ~~~~ #
    KCanvas.bind("<Button-1>",placeKamon)

    strg.pack()

def windowDetails():
    """ 
    name:   windowDetails
    brief:  Ajoute des éléments de détails graphiques sur le plateau
    param:  None 
    """
    global lastKamon, KJ1, KJ2, KCanvas, ident, idText
    
    # Graphic details on board
    Circle((Width/8), (Height/8), 'black', 'black')
    KJ1.append(ident)
    Circle((Width/8), ((Height*7)/8), 'white', 'white')
    KJ2.append(ident)
    idText=KCanvas.create_text((Width/8),(Height/8), text="J1", font= fontPlayer, fill='white')
    KJ1.append(idText)
    idText = KCanvas.create_text((Width/8),((Height*7)/8), text="J2", font= fontPlayer, fill='black')
    KJ2.append(idText)
    Circle(((Width*7)/8), (Height/8), beige, 'black')
    lastKamon.append(ident)
    idText = KCanvas.create_text(((Width*7)/8), (Height/8), text=None, font= fontPlayer, fill='black')
    lastKamon.append(idText)

def callBack():
    """ 
    name:   callBack
    brief:  Demande si le joueur veut quitter la partie
    param:  None
    """
    global KWindow
    
    res=messagebox.askyesno('Quitter','Êtes-vous sûr de vouloir quitter le jeu?')
    if res==True:
        KWindow.destroy()
    
def newGame():
    """ 
    name:   newGame
    brief:  Initialise une nouvelle instance du jeu
    param:  None 
    """
    if messagebox.askyesno('Nouvelle Partie','Souhaitez-vous créer une nouvelle partie?'):
        removeBoard()
        
def removeBoard():
    """ 
    name:   removeBoard
    brief:  Réinitialise les variables du jeu
    param:  None 
    """
    global KCanvas, keyKamon, KPlayable, listKamon, KJ1, KJ2, lastKamon, helpBool
    
    del listKamon[0:len(listKamon)]
    del lastKamon[0:len(lastKamon)]
    del KJ1[0:len(KJ1)]
    del KJ2[0:len(KJ2)]
    del KPlayable[0:len(KPlayable)]
    del tokenB[0:len(tokenB)]
    del tokenW[0:len(tokenW)]
    helpBool = False
    cptB, cptW = 0, 0
    KPlayable=[2,3,5,9,10,15,23,28,29,33,35,36]
    KCanvas.delete(ALL)
    play()
    windowDetails()
    ident = 0
    idText = 0
    
    
def us():
    """ 
    name:   us
    brief:  Ouvre une fenêtre About us
    param:  None 
    """
    us = tk.Tk()
    us.title("À Propos")
    label=Label(us, text="\nAuteurs: Elodie TRIBOUT, Axel VIDAL, Cédric PLUTA.\n==============================\n\n Version: 1.0.0\nDernière modification: 05/04/2016\n\n==============================\n\nLangage utilisé: Python 3.5.1\n\nLicence: Creative Common Licence BY-NC-SA.")
    label.pack()
    us.mainloop()
    
def rules():
    """ 
    name:   rules
    brief:  Ouvre une fenêtre sur les règles du jeu
    param:  None 
    """
    rul=tk.Tk()
    rul.title("Règles du jeu")
    txt1="\nDéroulement d'une partie :\n\n"
    txt2="Les 37 jetons KAMON sont disposés aléatoirement par les joueurs sur les différents nenuphars du plateau. Le jeton étoile est ensuite retiré. \n"
    txt3="Le premier joueur doit remplacer un des jetons présents au centre de chaque côté de l'hexagone par un de ses pions. \n"
    txt4="Le joueur suivant devra soit remplacer un jeton comprenant le même dessin, soit le même nombre de dessin par un de ses pions. Et ainsi de suite.\n"
    txtSep="#==================================================================================================#"
    txt5="\n\nConditions de victoire :`\n\n"
    txt6="Pour remporter la partie, un des deux joueurs doit, soit relier deux cotes opposés du plateau par un alignement de ses pions,\n"
    txt7="soit en faisant une boucle encerclant minimum une case, ou bien faire en sorte que le joueur adverse ne puisse plus jouer. \n"
    txt8="Si tous les pions KAMON sont retirés du plateau et qu'aucune des conditions de victoires n'est remplie par l'un des deux joueurs, \n"
    txt9="alors la partie est considerée comme nulle.\n"    
    label=Label(rul,text=txt1+txt2+txt3+txt4+txtSep+txt5+txt6+txt7+txt8+txt9, justify = LEFT, padx = 10)
    label.pack(side=TOP)
    rul.mainloop()
    

def Circle(x, y, color, outline):
    """ 
    name:   Circle
    brief:  Dessine un cercle de centre (x,y), de rayon r et avec une couleur en string
    param:  x: Abscisse; y: Ordonnée; color: Couleur du cercle 
    """
    global ident, KCanvas
    
    _x = x
    _y = y
    r = radius
    ident = KCanvas.create_oval(_x-r, _y-r, _x+r, _y+r,fill = color, outline = outline, width = 2)


def setGrid(sizeG):
    """
    name:   setGrid
    brief:  Trace une grille sur le canvas
    param:  sizeG: taille de l'espacement de la grille
    """
    sizeHW = 0
    while sizeHW < Height:
        # Drawn horizontal lines
        KCanvas.create_line(0, sizeHW, Height, sizeHW, fill = argent, width = 1)
        # Drawn vertical lines
        KCanvas.create_line(sizeHW, 0, sizeHW, Width, fill = argent, width = 1)
        sizeHW+=sizeG


def initCenter():
    """
    name:   initCenter
    brief:  Génère les nénuphars sur le plateau du Kamon
    param:  None
    """
    global centerX, centerY, centerList
    
    cX = centerX; cY = centerY
    key = 1
    j = 1
    k = 5
    moveX = radius
    moveY = 2*radius*(sqrt(3)/2)
    #nbCircles = (base * base) + (base * (base - 1)) + (base - 1)**2 
        # If base=4 then nbCircles = 37 
    while k <= 2*base:
        oldX = cX
        oldY = cY
        while j < k:
            centerList.append([cX,cY,key])
            cX+=2*radius
            j+=1
            key+=1
        k+=1
        j=1
        cX=oldX
        cY=oldY
        cX-=moveX
        cY+=moveY
    c2X = ((Width*31)/200)
    c2Y = (Height/2)
    c2X+= moveX
    c2Y+= moveY
    j = 1
    k = 7
    while k > base:
        oldX = c2X
        oldY = c2Y
        while j < k:
            centerList.append([c2X,c2Y,key])
            c2X += 2* radius
            j+=1
            key+=1
        k-= 1
        j = 1
        c2X = oldX
        c2Y = oldY
        c2X+= moveX
        c2Y+= moveY
        
def randomKamon():
    """ 
    name:   randonKamon
    brief:  Associe à chaque kamon une clé aléatoirement
    param:  None
    """
    global keyKamon
    
    #Kamon key generation
    keyKamon=random.sample(range(1,38),37)
    

def setKamon():
    """
    name:   setKamon
    brief:  Initialise les jetons Kamons
    param:  None
    """
    global listKamon, keyKamon, centerList
    
    number = 1
    Kkey = 1
 # Assignment color/number for each Kamon
    for color in Color:
        while number < 7:
            kamon=[color, number, Kkey]
            listKamon.append(kamon)
            number+=1
            Kkey+=1
        number = 1
    kamon=[beige, "étoile", Kkey]
    listKamon.append(kamon)
    randomKamon()

 # Kamons & random keys
    for i in range(37):
        listKamon[i][2]=keyKamon[i]
    for i in range(37):
        for j in range(37):
            if(listKamon[i][2] == centerList[j][2]):
                listKamon[i].append(centerList[j][0])
                listKamon[i].append(centerList[j][1])


def setBoard():
    """
    name:   setBoard
    brief:  Dispose les Kamon aléatoirement sur le plateau en fonction des clés attribuées par setKamon()
    param:  None
    """
    global listKamon, ident, idText, Kcanvas
    
    for i in range(nbCircles):
        Circle(listKamon[i][3], listKamon[i][4], listKamon[i][0], noirCnoir)
        listKamon[i].append(ident)
        idText = KCanvas.create_text(listKamon[i][3],listKamon[i][4], text= listKamon[i][1], font='Courrier 15 italic', fill= noirCnoir)
        listKamon[i].append(idText)
    helpGame('blue',4)
        

def placeKamon(event):
    """
    name:   placeKamon
    brief:  Moteur du jeu
    param:  event: clic gauche de souris 
    """
    global player, cptB, cptW, turn, Ktmp, lastKamon, KJ1, KJ2, KCanvas, helpBool, graph
    
    strg.configure(text="X= "+str(event.x)+" ~~ Y="+str(event.y))
    _x = event.x
    _y = event.y
        
    for circle in range(len(listKamon)):
        if((((_x-listKamon[circle][3])**2 + (_y-listKamon[circle][4])**2) < radius**2) & (listKamon[circle][1] != 'étoile')):
            if(isKPlayable(listKamon[circle][2]) & isInTokenB(listKamon[circle][2]) & isInTokenW(listKamon[circle][2])):    
                # Update: Last kamon played
                for i in range(len(listKamon[circle])):
                    Ktmp.append(listKamon[circle][i])    
                # Update: playable tokens
                helpGame(noirCnoir,2)
                UpDKPlayables()

                # Graphic Update : Last kamon played
                KCanvas.itemconfig(lastKamon[0], fill=Ktmp[0])
                KCanvas.itemconfig(lastKamon[1], text=Ktmp[1])
                
                
                del Ktmp[0:len(Ktmp)]
                
                blockOpponent()
                makeTree()
                
                
                # Placement of player's token
                if((player == 'black') & (cptB <= 18)):
                    KCanvas.itemconfig(KJ1[0], outline='black')
                    KCanvas.itemconfig(KJ2[0], outline='red')
                    KCanvas.itemconfig(listKamon[circle][5], fill='black')
                    KCanvas.delete(listKamon[circle][6])
                    tokenB.append(listKamon[circle][2])
                    listKamon[circle][0]='black'
                    listKamon[circle][1]=None           
                    player = 'white'
                    cptB+=1
                    KCanvas.delete(KWindow, turn)
                    turn = KCanvas.create_text((Width/2), ((Height*15)/16), text = "Tour du joueur Blanc", font= "Arial 13 italic", fill ='black')
                else:
                    if ((player == 'white') & (cptW <= 18)):
                        KCanvas.itemconfig(KJ1[0], outline='red')
                        KCanvas.itemconfig(KJ2[0], outline='white')
                        KCanvas.itemconfig(listKamon[circle][5], fill='white')
                        KCanvas.delete(listKamon[circle][6])
                        tokenW.append(listKamon[circle][2])
                        player = 'black'
                        cptW+=1
                        listKamon[circle][0]='white'
                        listKamon[circle][1]=None
                        KCanvas.delete(KWindow, turn)
                        turn = KCanvas.create_text((Width/2), ((Height*15)/16), text = "Tour du joueur Noir", font= "Arial 13 italic", fill ='black')
                # Checking victory conditions
                #grapheN()
                boucle()
        

def helpGame(color, wid):
    """ 
    name:   HelpGame
    brief:  Configure les cercles qui sont jouables
    param:  color: couleur de la bordure, wod: epaisseur du trait de bordure 
    """
    global KCanvas
    
    for i in range(len(listKamon)):
                    if(isKPlayable(listKamon[i][2]) & (listKamon[i][1]!='étoile')):
                        KCanvas.itemconfig(listKamon[i][5], outline=color, width = wid)
                        KCanvas.itemconfig(listKamon[i][6], fill=noirCnoir)

def isKPlayable(num):
    """
    name:   isKPlayable
    brief:  Vérifie si le jeton cliqué est jouable ou non
    param:  Clé id du cercle cliqué
    """
    for i in KPlayable:
        if num == i:
            return True
    return False;

def isInTokenB(num):
    """ 
    name:   isInTokenB
    brief:  Renvoie Faux si le jeton cliqué est dans la liste des pions déjà joués par le joueur noir, Vrai sinon
    param:  Clé id du cercle cliqué 
    """
    for i in range(len(tokenB)):
        if (tokenB[i] == num):
            return False
    return True

def isInTokenW(num):
    """ 
    name:   isInTokenW
    brief:  Renvoie Faux si le jeton cliqué est dans la liste des pions déjà joués par le joueur blanc, Vrai sinon
    param:  Clé id du cercle cliqué 
    """
    for i in range(len(tokenW)):
        if (tokenW[i] == num):
            return False
    return True


def UpDKPlayables():
    """
    name:   UpDKPlayables
    brief:  Met à jour la liste des jetons kamons jouables
    param:  None
    """
    global Ktmp, listKamon, KPlayable, helpBool
    
    del KPlayable[0:len(KPlayable)]
    for i in range(len(listKamon)):
        if ((Ktmp[0] == listKamon[i][0]) | (Ktmp[1] == listKamon[i][1])):
                KPlayable.append(listKamon[i][2])
    if helpBool:
        helpGame('blue',4)
        
def blockOpponent():
    """
    name:   blockOpponent
    brief:  Affiche la victoire d'un joueur en cas de blocage de l'adversaire
    param:  None
    """
    if(len(KPlayable)==1):
        vict = tk.Tk()
        txt="Victoire du joueur "+player
        label = Label(vict, text=txt, font='Verdana 30')
        label.pack()

def initGraph():
    """
    name:   initGraph
    brief:  Crée un graphe non-orienté du plateau
    param:  None
    """
    global listKamon, nodeList, graph

    
    # Search of coordinates(x,y) from 1 to 37
    i = 1
    while i <= 37:
        for j in range(len(listKamon)):
            if(listKamon[j][2] == i):
                nodeList.append([listKamon[j][3],listKamon[j][4]])
                i+=1
    i = 0
    for node in nodeList:
        neighbs = neighbors(node)
        for kamon in listKamon:
            if (node[0] == kamon[3]) & (node[1] == kamon[4]):
                graph.append([kamon[2], kamon[0], len(neighbs)])
                graph[i].append(neighbs)
        i+=1
    print(graph)

def graphB():
    """
    name:   graphB
    brief:  Construit un graphe non-orienté des jetons noirs
    param:  None
    """
    global graph
    
    victBlack = []
    i = 1
    j = 0
    while i <= 37:
        victBlack.append([i])
        victBlack[i-1].append([])
        i+=1

    for node in graph:
        for neighbour in node[1]:
            if(neighbour in tokenB):
                victBlack[j][1].append(neighbour)
        j+=1
    print(victBlack, "\n\n")

def graphW():
    """
    name:   graphW
    brief:  Construit un graphe non-orienté des jetons blancs
    param:  None
    """
    global graph
    
    victWhite = []
    i = 1
    j = 0
    while i <= 37:
        victWhite.append([i])
        victWhite[i-1].append([])
        i+=1

    for node in graph:
        for neighbour in node[1]:
            if(neighbour in tokenW):
                victWhite[j][1].append(neighbour)
        j+=1
    print("\n\n\n", victWhite, "\n\n")


def neighbors(node):
    """
    name:   neighbors
    brief:  Met à jour la liste voisins d'un pion
    param:  node: pion du plateau
    """
    dirs =[[2*radius, 0], [radius, 2*radius*(sqrt(3)/2)], [-radius, 2*radius*(sqrt(3)/2)], [-2*radius, 0], [-radius, -2*radius*(sqrt(3)/2)], [radius, -2*radius*(sqrt(3)/2)]] #6 directions possibles de voisins
    result =[]
    for dir in dirs:
        neighbor = [node[0] + dir[0], node[1] + dir[1]]
        if neighbor in nodeList:
            for kamon in listKamon:
                if (neighbor[0] == kamon[3]) & (neighbor[1] == kamon[4]):
                    result.append(kamon[2])
            
    return result

"""
    def ligne():
    global player, listKamon, graphe, grapheN, grapheB
    grapheN = grapheB = graphe
    
    if(player == 'white'):
        for node in grapheN:
            for neighbor in node[1]:
                if(neighbor in pieceN):
                    node[1].remove(neighbor)
    if(player == 'black'):
        for node in grapheB:
            for neighbor in node[1]:
                if(neighbor in pieceB):
                    node[1].remove(neighbor)
    print("graphe noir : ", grapheN)
    print("graphe blanc : ", grapheB, "\n")

    def ligne():
    global player, graphe, grapheN, grapheB
    grapheN = grapheB = graphe

    if(player == 'white'):
        for node in range(len(graphe)):
            for neighbor in range(len(graphe[node][1])):
                print(neighbor)
                if(graphe[node][1][neighbor] not in pieceN):
                    grapheN[node][1].remove(graphe[node][1][neighbor])
                    print("graphe Noir : ", grapheN)
"""

def boucle():
    """
    name:   boucle
    brief:  Cherche si il existe des cycles dans le graphe
    param:  None
    """
    global player, listKamon, graphe

    cpt = 0

    if(player == 'black'):
        for node in graph:
            if(len(node[1]) == 6):
                for neighbor in node[1]:
                    for kamon in listKamon:
                        if (neighbor == kamon[2]) & (kamon[0] == 'black'):
                            cpt+=1
        if(cpt == 6):
            vict = tk.Tk()
            txt="Victoire du joueur "+player
            label = Label(vict, text=txt, font='Verdana 30')
            label.pack()
            
    if(player == 'white'):
        for node in graph:
            if(len(node[1]) == 6):
                for neighbor in node[1]:
                    for kamon in listKamon:
                        if (neighbor == kamon[2]) & (kamon[0] == 'white'):
                            cpt+=1
        if(cpt == 6):
            vict = tk.Tk()
            txt="Victoire du joueur "+player
            label = Label(vict, text=txt, font='Verdana 30')
            label.pack()

#=======================================#
# Implementation of a class Tree        #
# --------------------------------------#
# Generic n-ary Tree                    #
#=======================================#

class Tree:
    """
    Arbre générique n-aire
    """

    def __init__(self, parent, value = None):
        """
        Constructeur paramétré de l'Arbre
        """
   
        self.parent = parent
        self.value = value
        self.vortexList = []

        if parent is None:
            self.birthOrder = 0
        else:
            self.birthOrder = len(parent.vortexList)
            pere.vortexList.append(self)

    def getNbChild(self):
        """
        Retourne le nombre d'enfants de self
        """
        return len(self.vortexList)

    def getChild(self, n):
        """
        Retourne le n-ième sommet de l'Arbre
        """
        return self.vortexList[n]

    def allPath(self):
        result = []
        parent = self.parent
        child = self

        while parent:
            result.insert(0, child.birthOrder)
            parent, child = parent.parent, parent

        return result

    def nodeId(self):
        """
        Retourne le chemin jusqu'à self dans l'Arbre
        """
        return NodeId(allPath)

def makeTree():
    global listKamon, sideList, player
    
    TreeTab =[]
    
    for i in sideList:
        for j in i:
            for kamon in listKamon:
                if(j == kamon[2]):
                    _x = kamon[3]
                    _y = kamon[4]
                    Tree_aux = Tree(None, j)
                    TreeTab.append(Tree_aux)
                    k = 0
                    for circle in range(len(listKamon)):
                        if((((_x-listKamon[circle][3]**2) + (_y-listKamon[circle][4])**2) < 2*radius**2) & (listKamon[circle][1] != 'étoile') & (listKamon[circle][0] == player)):
                            Tree_aux.vortexList.append(listKamon[circle][2])   
                    k+=1

    return TreeTab
    
#=======================================#
# Implementation of a class NodeId      #
# --------------------------------------#
# Localisation of a node in a Tree as a #
# path since the root                   #
#=======================================#

class NodeId:
    """
    Localisation d'un noeud dans un Arbre
    tel un chemin depuis la racine
    """

    def __init__(self, path):
        """
        Constructeur de l'objet NodeId
        """
        self.path = path

    def __str__(self):
        L = map(str, self.chemin)
        return string.join (L, "/")

    def find(self, node):
        return self.__reFind(node, 0)

    def __reFind(self, node, i):
        if i>= len(self.path):
            return node.value
        else:
            childNo = self.path[i]

        try:
            child = node.getChild(childNo)
        except IndexError:
            return None
        return self.__reFind(child, i+1)

    def isOnPath(self, node):
        nodePath = node.allPath()
        if len(nodePath) > len(self.path):
            return 0
        for i in range(len(nodePath)):
            if nodePath[i] != self.path[i]:
                return 0
        return 1

########################################
#                Main                  #
########################################




    # Initialization of the game #


print("########################################")
print("# Program: Kamon                       #")
print("# Version: 1.0.0                       #")
print("# Last Update: 2016/04/05              #")
print("########################################\n\n")


initWindow()
play()
#setGrid(28)
print("Graphe du plateau : \n")
initGraph()
windowDetails()

    #  ~~~~  Event loop  ~~~~  #

KWindow.mainloop()
