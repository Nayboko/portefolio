# Les clefs du temps

## Résumé

Ce jeu a été réalisé lors de ma première Game Jam de 24 heures, l'EduGameJam, organisé par le Ministère de l'Education Nationale et le réseau Canopé.

Par équipe, chaque jeu devait répondre à l'un des thèmes du socle commun Primaire-Collège-Lycée. Nous avions choisi les suivants :
* méthodes et outils de l'apprentissage ;
* systèmes naturels et techniques ;
* représentation du monde.

En découle un Point'n Click destiné à un public de 10 à 15 ans où l'on explore des tableaux de différentes époques tout en relevant les anachronismes qui ont pu s'y glisser. Angus, un chien voyageur du temps, fournira plusieurs anecdotes et questions afin de développer la curiosité de l'utilisateur.

## Comment jouer ?

Le but va être de parcourir des tableaux, les uns après les autres et de les en nettoyer des objets insolites. On découvre différentes oeuvres auxquelles on peut y rattacher un contexte historique selon certains objets qui ne sont pas anachroniques.

Il suffit alors d'effectuer des clics gauches sur les objets disposés sur le tableau, de même sur les questions posées par Angus. Des feedbacks sonores permettent de déterminer si la réponse donnée ou l'objet cliqué répond à différents critères de validité.

Un système d'aide permet de mettre en évidence un objet de la scène jugée insolite.

## Implémentation

Le projet a été développé avec le logiciel propriétaire "Construct 2", autorisant plus de souplesse niveau implémentation pour se concentrer sur les éléments de Game  Design.

En résulte une exportation HTML/CSS/JS pouvant être exécuté, quelque soit le support de jeu, sur un navigateur.

## Les améliorations envisageables

* implémenter avec un moteur différent (Python/Pygame, C#/Unity, ...) ;
* développer le contenu et ajouter de nouvelles oeuvres d'autres époques ;
* permettre aux enseignants de faire leur propre tableaux avec un éditeur.


## Exécuter

La dernière version du projet se trouve dans le dossier "ClefsTemps_v3.0". Pour l'exécuter en local, il faut déposer le dossier sur un webserveur (par exemple [Servez](https://greggman.github.io/servez/)) puis lancer "[127.0.0.1:8080/index.html](127.0.0.1:8080/index.html)" sur un navigateur.

## Démonstration vidéo

Dans le dossier "Démonstration" se trouve la vidéo en question
